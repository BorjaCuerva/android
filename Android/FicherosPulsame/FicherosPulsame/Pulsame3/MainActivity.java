package com.example.usuario.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


// import android.app.Activity;

import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        // Configuramos la ventana, añadiendo un botón
        // que llamará a nuestro método protegido.

    }

    /**
     * Método llamado cuando se pulsa sobre el botón
     * de la ventana. Es llamado a través de la clase
     * anónima del evento.
     */
    public void botonPulsado(View v) {

        // Incrementamos el contador...
        ++_numVeces;
        Button b = (Button) v;
        // ... y actualizamos la etiqueta del botón.
        b.setText("Pulsado " + _numVeces + " veces");

    } // botonPulsado

    /**
     * Número de veces que se ha pulsado el botón.
     */
    private int _numVeces;

}
