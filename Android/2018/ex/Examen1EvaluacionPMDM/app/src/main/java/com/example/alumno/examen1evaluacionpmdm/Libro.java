package com.example.alumno.examen1evaluacionpmdm;

/**
 * Created by Alumno on 14/12/2016.
 */

public class Libro {
    private int id;
    private String titulo;
    private String autor;
    private int portada;
    private int favorito;

    public Libro()
    {
        id=0;
        titulo="";
        autor="";
        portada=0;
        favorito=0;

    }
    public Libro(int id,String titulo,String autor, int portada,int favorito)
    {
        this.id=id;
        this.titulo=titulo;
        this.autor=autor;
        this.portada=portada;
        this.favorito=favorito;

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public void setPortada(int portada) {
        this.portada = portada;
    }

    public int getId() {

        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getAutor() {
        return autor;
    }

    public int getPortada() {
        return portada;
    }

    public int getFavorito() {
        return favorito;
    }

}
