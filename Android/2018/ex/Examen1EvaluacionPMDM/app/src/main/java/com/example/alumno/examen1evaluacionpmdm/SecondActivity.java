package com.example.alumno.examen1evaluacionpmdm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class SecondActivity extends AppCompatActivity {

    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        String nombre=getIntent().getStringExtra("Nombre");
        setTitle(nombre);
        String foto="@drawable/p"+getIntent().getIntExtra("foto",0);
        iv=(ImageView) findViewById(R.id.imageViewSecond);
        int fotito=this.getResources().getIdentifier(foto,"drawable","com.example.alumno.examen1evaluacionpmdm");
        iv.setImageResource(fotito);


    }
}
