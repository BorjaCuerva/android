package com.example.biblioteca;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class BDAdapter extends ArrayAdapter<Libro> {

    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_CATEGORIA = "categoria";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    private static final String TABLA_BD = "notas";
    private Context context;
    private SQLiteDatabase basedatos;
    private BDHelper bdHelper;
    private String spinete;
    private int valor;


    public BDAdapter(Context context, ArrayList<Libro> datos) {
        super(context, R.layout.list_item, datos);
        this.context = context;
        bdHelper = new BDHelper(context, "DBLibros", null, 1);
        basedatos = bdHelper.getWritableDatabase();
    }

    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.list_item, null);

        //Mediante getItem cargamos cada uno de los objetos del array
        Libro mielemento = getItem(position);


        TextView tvTitulo = (TextView) item.findViewById(R.id.textViewTitulo);
        TextView tvTipo = (TextView) item.findViewById(R.id.textViewAutor);
        ImageView imagenView = (ImageView) item.findViewById(R.id.imageViewCuadrado);

        tvTitulo.setText(mielemento.getTitulo());
        tvTipo.setText(mielemento.getAutor());
        int portada = mielemento.getPortada();
        if (portada == 0) {
            imagenView.setImageResource(R.drawable.tachar);
        } else {
            imagenView.setImageResource(R.drawable.sin_tachar);
        }

        if (mielemento.getFavorito() == 1) {
            tvTitulo.setPaintFlags(tvTitulo.getPaintFlags() |
                    Paint.STRIKE_THRU_TEXT_FLAG);
            tvTitulo.setTextColor(Color.parseColor("#00FF00"));
        } else {
            tvTitulo.setPaintFlags(tvTitulo.getPaintFlags()
                    & ~Paint.STRIKE_THRU_TEXT_FLAG);
            tvTitulo.setTextColor(Color.parseColor("#FF0000"));
        }


        // Devolvemos la Vista (nueva o reutilizada) que dibuja la opcion
        return (item);
    }

    public BDAdapter abrir() throws SQLException {
        BDAdapter a = null;

        return a;
    }

    public void cerrar() {
        bdHelper.close();
    }

    public boolean actualizarNota(ArrayList<Libro> al) {
        boolean sw = false;
        try {
            for (int i = 0; i < al.size(); i++) {
                ContentValues valores = new ContentValues();
                valores.put("FAVORITO", al.get(i).getFavorito());
                basedatos.update("LIBROS", valores, "_id=" + al.get(i).getId(), null);

            }
            sw = true;
        } catch (SQLException e) {
            sw = false;
        }

        return sw;
    }

    public boolean borrarNota(long id) {
        boolean sw = false;

        return sw;
    }


}