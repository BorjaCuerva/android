package com.example.biblioteca;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        String nombre = getIntent().getStringExtra("Nombre");
        setTitle(nombre);
        String foto = "@drawable/p" + getIntent().getIntExtra("foto", 0);
        iv = (ImageView) findViewById(R.id.imageViewSecond);
        int fotito = getApplicationContext().getResources().getIdentifier(foto, "drawable", getApplicationContext().getPackageName());
        iv.setImageResource(fotito);


    }
}
