package com.example.biblioteca;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db = null;
    private ArrayList<Libro> al = null;
    private BDAdapter adaptador = null;
    private ListView lv;
    private BDHelper bdnh = null;

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bdnh = new BDHelper(this, "DBLibros", null, 1);
        db = bdnh.getWritableDatabase();

        Spinner prueba = (Spinner) findViewById(R.id.spinner);

        //Creamos el adaptador
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.librillos, android.R.layout.simple_spinner_item);
//Añadimos el layout para el menú
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Le indicamos al spinner el adaptador a usar
        prueba.setAdapter(adapter);


        prueba.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0) {
                    al = null;
                    al = findAllAutor();
                    lv = (ListView) findViewById(R.id.listViewLibros);
                    adaptador = new BDAdapter(view.getContext(), al);
                    lv.setAdapter(adaptador);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                            ;
                            if (al.get(position).getFavorito() == 0) {
                                al.get(position).setFavorito(1);
                            } else {
                                al.get(position).setFavorito(0);
                                Toast.makeText(v.getContext(), "Has marcado como favorito: " + al.get(position).getTitulo(), Toast.LENGTH_SHORT).show();
                            }
                            lv.setAdapter(adaptador);
                            adaptador.notifyDataSetChanged();

                        }


                    });
                    registerForContextMenu(lv);
                } else {
                    al = null;
                    al = findAllLibro();
                    lv = (ListView) findViewById(R.id.listViewLibros);
                    adaptador = new BDAdapter(view.getContext(), al);
                    lv.setAdapter(adaptador);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                            ;
                            if (al.get(position).getFavorito() == 0) {
                                al.get(position).setFavorito(1);
                                //adaptador.actualizarNota(al.get(position).getId(),al.get(position).getFavorito());
                            } else {
                                al.get(position).setFavorito(0);
                                Toast.makeText(v.getContext(), "Has marcado como favorito: " + al.get(position).getTitulo(), Toast.LENGTH_SHORT).show();
                                //adaptador.actualizarNota(al.get(position).getId(),al.get(position).getFavorito());
                            }
                            lv.setAdapter(adaptador);
                            //adaptador.notifyDataSetChanged();

                        }


                    });
                    registerForContextMenu(lv);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // vacio

            }
        });


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.MenuOp1:
                boolean sw = adaptador.actualizarNota(al);
                if (sw) {
                    Toast.makeText(this, "Base de datos actualizada", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Estoy fallando", Toast.LENGTH_SHORT).show();
                }

                return true;

        }


        return false;

    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater = getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos
        // el fichero XML correspondiente
        if (v.getId() == R.id.listViewLibros) {
            AdapterView.AdapterContextMenuInfo info =
                    (AdapterView.AdapterContextMenuInfo) menuInfo;
            // Definimos la cabecera del menú contextual
            menu.setHeaderTitle("Operaciones sobre: " + al.get(info.position).getTitulo());
            inflater.inflate(R.menu.menu_context, menu);
            if (al.get(info.position).getPortada() == 0) {
                menu.findItem(R.id.borrarMenu).setTitle("No tiene portada");
                menu.findItem(R.id.borrarMenu).setEnabled(false);
            } else {
                menu.findItem(R.id.borrarMenu).setTitle("Ver portada");
                menu.findItem(R.id.borrarMenu).setEnabled(true);
            }

            System.out.println(al.get(info.position).getPortada());

        }
    }

    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        EditText et = (EditText) findViewById(R.id.borrarMenu);

        switch (item.getItemId()) {


            case R.id.borrarMenu:
                Intent i = new Intent(this, SecondActivity.class);
                i.putExtra("Nombre", al.get(info.position).getTitulo());
                i.putExtra("foto", al.get(info.position).getId());
                startActivity(i);


                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public ArrayList<Libro> findAllAutor() {
        ArrayList<Libro> al = new ArrayList<Libro>();
        BDHelper bdnh = new BDHelper(this, "DBLibros", null, 1);
        SQLiteDatabase db = bdnh.getWritableDatabase();
        Cursor cursor = db.rawQuery("select _id, titulo, autor, portada, favorito from LIBROS order by AUTOR",
                null
        );
        while (cursor.moveToNext()) {
            al.add(new
                    Libro(cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getString(cursor.getColumnIndex("TITULO")),
                    cursor.getString(cursor.getColumnIndex("AUTOR")),
                    cursor.getInt(cursor.getColumnIndex("PORTADA")),
                    cursor.getInt(cursor.getColumnIndex("FAVORITO"))));
        }
        return al;
    }

    public ArrayList<Libro> findAllLibro() {
        ArrayList<Libro> al = new ArrayList<Libro>();
        BDHelper bdnh = new BDHelper(this, "DBLibros", null, 1);
        SQLiteDatabase db = bdnh.getWritableDatabase();
        Cursor cursor = db.rawQuery("select _id, titulo, autor, portada, favorito from LIBROS order by TITULO",
                null
        );
        while (cursor.moveToNext()) {
            al.add(new
                    Libro(cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getString(cursor.getColumnIndex("TITULO")),
                    cursor.getString(cursor.getColumnIndex("AUTOR")),
                    cursor.getInt(cursor.getColumnIndex("PORTADA")),
                    cursor.getInt(cursor.getColumnIndex("FAVORITO"))));
        }
        return al;
    }
}