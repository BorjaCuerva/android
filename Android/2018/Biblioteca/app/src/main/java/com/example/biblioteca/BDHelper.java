package com.example.biblioteca;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Borja
 */

public class BDHelper extends SQLiteOpenHelper {

    private static final String BD_NOMBRE="BDLibros.db";
    private static final int BD_VERSION=1;
    Context contexto;

    public BDHelper(Context contexto, String nombre,
                    SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
        this.contexto=contexto;
    }
    public void onCreate(SQLiteDatabase database)
    {
        try
        {
            // La estructura de la base de datos se define dentro del fichero bdnombres.sql
            // Cargamos tambi�n datos en la base de datos
            InputStream ficheroraw = contexto.getResources().openRawResource(R.raw.tituloautor);
            BufferedReader bfr = new BufferedReader(new InputStreamReader(ficheroraw));
            String auxStr = "";
            while ((auxStr = bfr.readLine()) != null){
                database.execSQL(auxStr);
            } // end while
            bfr.close();
        }
        catch (SQLException e) {
            Log.e("Error al crear BD", e.toString());
            throw e;
        } catch (IOException e) {
            Log.e("Error al leer los datos", e.toString());
        }
    }
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int versionNueva)
    {

    }

}