package com.example.biblioteca;

/**
 * Created by Borja
 */

public class Libro {
    private int id;
    private String titulo;
    private String autor;
    private int portada;
    private int favorito;

    public Libro() {
        id = 0;
        titulo = "";
        autor = "";
        portada = 0;
        favorito = 0;

    }

    public Libro(int id, String titulo, String autor, int portada, int favorito) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.portada = portada;
        this.favorito = favorito;

    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getPortada() {
        return portada;
    }

    public void setPortada(int portada) {
        this.portada = portada;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

}
