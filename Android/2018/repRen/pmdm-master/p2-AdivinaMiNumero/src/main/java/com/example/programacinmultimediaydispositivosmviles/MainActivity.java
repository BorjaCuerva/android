package com.example.programacinmultimediaydispositivosmviles;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView textView_principal;//Definimos el text View superior
    TextView textextView_intentos;//Definimos el text View inferior
    EditText editText_NumBox;//Definimos el Edit Text
    Button boton;//Definimos el boton
    int num_secreto;//Definimos numero secreto "Valor random"
    String mensaje;
    int intento;
    boolean finDeJuego;//Definimos variable de control del estado de juego
    Resources res;//Definimos var de acceso a recursos
    int intentos;//Definimos num de intentos del juego

    final static int NUM_MAXIMO =100;//Número máximo del juego, opción de parametrización del juego


    /**
     * Metodo creado para establecer valores a los paremetros del juego
     * y establecer la pantalla inicial
     */
    public void startMainActivity() {
        num_secreto = new java.util.Random().nextInt(NUM_MAXIMO+1);//Generador del numero aleatorio
        //this.num_secreto = 40;//DEBUG
        this.finDeJuego = false;//Estado del juego a false = "no finalizado"
        this.intentos = 0;//Establecer intentos a zero
        textextView_intentos.setText(res.getQuantityString(R.plurals.intentalo, intentos + 1, intentos));//Escribimos mensaje en el textView secundario
        boton.setText(R.string.s_probar);//Establecemos el mensaje escrito en el boton
        textView_principal.setText(String.format(res.getString(R.string.s_primerMensaje), NUM_MAXIMO));//Mensaje al empezar la aplicación
        editText_NumBox.setVisibility(View.VISIBLE);//Pasamos el estado del editText a visible --> Solo funciona una vez reiniciada la partida
        textextView_intentos.setVisibility(View.VISIBLE);//Pasamos el estado del textView a visible --> Solo funciona una vez reiniciada la partida
    }

    /**
     * Metodo para instanciar los objetos de la aplicacion
     */
    private void instanciador() {
        res = getResources();//Gestionador de recursos
        textView_principal = findViewById(R.id.textView_top);//Instanciamos la caja de texto superior
        textextView_intentos = findViewById(R.id.textView_intentalo);//Instanciando Text View inferior
        editText_NumBox = findViewById(R.id.editText_cajaNumeros);//Instanciando caja de numeros
        boton = findViewById(R.id.button_pulsador);//Instanciamos el boton
    }

    /**
     * Metodo que guardara el estado de la aplicacion al girar la pantalla
     * @param outInstance
     */
    @Override
    public void onSaveInstanceState(Bundle outInstance) {
        super.onSaveInstanceState(outInstance);

        outInstance.putString("STATE_MENSAJE", this.mensaje);//
        outInstance.putInt("STATE_INTENTO", this.intento);//
        outInstance.putInt("STATE_NUMERO_SECRETO", this.num_secreto);//NUM RANDOM GUARDADO
        outInstance.putInt("STATE_NUM_INTENTOS", this.intentos);//Número de intentos guardado
        outInstance.putBoolean("STATE_FIN_DE_JUEGO", this.finDeJuego);//ESTADO DE JUEGO GUARDADO
    }

    /**
     * Metodo que restaurara el estado de la aplicacion al girar la pantalla
     * @param savedInstance
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstance) {
        super.onRestoreInstanceState(savedInstance);



        this.mensaje = savedInstance.getString("STATE_MENSAJE");//
        this.intento = savedInstance.getInt("STATE_INTENTO");//

        this.intentos = savedInstance.getInt("STATE_NUM_INTENTOS");//Números de intentos restaurados
        this.num_secreto = savedInstance.getInt("STATE_NUMERO_SECRETO");//Número aleatorio restaurado
        this.finDeJuego = savedInstance.getBoolean("STATE_FIN_DE_JUEGO");//Fin del juego restaurado
        textView_principal.setText(String.format(mensaje, intento));
        textextView_intentos.setText(res.getQuantityString(R.plurals.intentalo, intentos + 1, intentos));

        if (finDeJuego) {//Si el juego esta terminado llamaremos al medodo esVictoria()
            esVictoria();
        }
    }

    /**
     * Metodo principal que dara inicio al juego
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instanciador();//INSTANCIANDO OBJETOS
        startMainActivity();//CARGANDO DATOS DEL JUEGO

        /*
        LISTENER DEL BOTON PROBAR/Jugar otra vez A LA ESCUCHA
         */
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                botonProbar();//Metodo llamado al pulsar el boton probar/Jugar otra vez
            }
        });
    }

    /**
     * Metodo que sera llamado por el metodo onClick del unico boton de este juego
     * este metodo es el que controlara gran parte del juego
     */
    private void botonProbar() {
        if (!editText_NumBox.getText().toString().equals("")) {//Comprueba que el edit text no este vacio
            if (!finDeJuego) {//verifica si el juego se ha terminado
                int valor_Recibido = Integer.valueOf(editText_NumBox.getText().toString());//convertimos el valor introducido en el campo edit text en integer
                intentos++;//incrementamos la variable que almacena los intentos
                if (valor_Recibido == num_secreto) {//comprueba que si el num generado aletariamente es igual al valor recibido por teclado, si lo es se termina el juego
                    esVictoria();//llamada al metodo victoria
                } else if (valor_Recibido > num_secreto) {//El número es menor
                    elNumeroEs(valor_Recibido, R.string.s_esMenor);//pasamos como parametro el mensaje de es menor
                } else {//El número es mayor
                    elNumeroEs(valor_Recibido, R.string.s_esMayor);//pasamos como parametro el mensaje de es mayor

                }
            } else {
                startMainActivity();//Volvemos a iniciar el juego
            }
        }
    }

    /**
     * El numero puede ser mayor o menor que el introducido por el usuario, este metodo es el que va a personalizar dichos mensajes.
     *
     * @param valor_Recibido
     * @param referenciaTexto
     */
    private void elNumeroEs(int valor_Recibido, int referenciaTexto) {
        intento = valor_Recibido;
        mensaje = res.getString(referenciaTexto);
        textView_principal.setText(String.format(mensaje, valor_Recibido));//capturamos el mensaje de strings.xml yescribimos el mensaje en el textView principal "top"
        textextView_intentos.setText(res.getQuantityString(R.plurals.intentalo, intentos + 1, intentos));//Actuliza el mensaje del text view secundario "bottom-left"
    }

    /**
     * Metodo que sera llamado en caso de victoria
     */
    private void esVictoria() {
        textView_principal.setText(String.format(res.getString(R.string.s_victoria), intentos));//capturamos el mensaje de victoria en strings.xml y escribimos el mensaje de victoria en el textView principal "top"
        boton.setText(R.string.s_jugarOtraVez);//cambia el mensaje del boton, pasa a ser "JUGAR OTRA VEZ"
        textextView_intentos.setVisibility(View.INVISIBLE);//Ponemos el text view secundario invisible, para que no sea visto
        editText_NumBox.setVisibility(View.INVISIBLE);//Ponemos el edit text invisible, para que no pueda recibir texto
        finDeJuego = true;//pasamos el estado de juego a true = finalizado
    }


}
