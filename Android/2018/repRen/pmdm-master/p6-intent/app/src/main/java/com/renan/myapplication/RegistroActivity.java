package com.renan.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    EditText editText_nombre;//Declaramos el editText nombre de manera local
    EditText editText_apellido;//Declaramos el editText apellido de manera local
    Boolean modificar;//Declaramos el boolean modificar de manera local

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Instanciamos el EditText nombre
        editText_nombre = findViewById(R.id.editText_nombre);
        //Instanciamos el EditText apellido
        editText_apellido = findViewById(R.id.editText_apellido);

        //Asignamos a la variable modificar su valor que dependerá de la activity
        //principal, esto controlará desde que botón ha sido llamado
        modificar = getIntent().getExtras().getBoolean("modificar");

        //En caso de que modificar sea true, al editText se le asignarán sus debidos valores
        if (modificar) {
            //Instanciamos el text view superior para modificar su mensaje
            TextView textViewSuperior = findViewById(R.id.textView);
            //Modificamos el mensaje del text view superior
            textViewSuperior.setText(R.string.menu_modificar);
            //Asignamos valor al EditText nombre
            editText_nombre.setText(getIntent().getExtras().getString("nombre"));
            //Asignamos valor al EditText apellido
            editText_apellido.setText(getIntent().getExtras().getString("apellido"));
        }

        //Instanciamos el botón aceptar
        Button boton_aceptar = findViewById(R.id.button_registro_aceptar);
        //Botón aceptar a la escucha
        boton_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //llamada al método aceptar
                aceptar(v);
            }
        });
    }

    /*
    Método que va a comprobar y válidar los datos introducidos por el usuario
     */
    void aceptar(View v) {
        //Manejador de excepciones para atrapar excepciones provenientes de la comprobación del nombre o apellido
        try {
            //Validando el nombre y posteriormente asignanodo su valor a la variable nombre
            String nombre = Recursos.verificarNombre(editText_nombre.getText().toString());
            //Validando el apellido y posteriormente asignanodo su valor a la variable apellido
            String apellido = Recursos.verificarApellido(editText_apellido.getText().toString());

            //Declaración de la intención
            Intent datos = new Intent();
            //Añadimos al almacén de datos el nombre
            datos.putExtra("nombre", nombre);
            //Añadimos al almacén de datos el apellido
            datos.putExtra("apellido", apellido);
            //Añadimos al almacén de datos el booleano de sí ha sido una modificación
            datos.putExtra("modificar", modificar);

            //Decimos al programa que la actividad está lista para finalizar
            setResult(RESULT_OK, datos);
            //Finalizamos la actividad
            finish();

            //Manejador de las excepciones de tipo IllegalArgumentException
        } catch (IllegalArgumentException e) {
            //Mensaje de aviso al usuario de que algo no va correctamente.
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
