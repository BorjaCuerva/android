package com.renan.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Referencia al layout linear principal de activity_main
    LinearLayout contenedorPadre;
    //TextView nombre, para cuando haya que poner datos
    TextView tvNombre;
    //TextView apellido, para cuando haya que poner datos
    TextView tvApellido;
    //Declarando el botón modificar de manera local
    Button botonModificar;
    //Declarando el botón alta de manera local
    Button botonAlta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instanciando el botón de alta
        botonAlta = findViewById(R.id.button_alta);
        //Instanciando el layout linear para trabajar con sus views
        contenedorPadre = findViewById(R.id.linear_layout);

        //Botón alta en escucha
        botonAlta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LLamada al método onclick()
                onclick(false);
            }
        });

        //Instanciando el botón de modificar
        botonModificar = findViewById(R.id.button_modificar);

        //Botón modificar en escucha
        botonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LLamada al método onclick()
                onclick(true);
            }
        });

    }

    /**
     * Método encargado de llamar a la nueva Activity y pasarle los parámetros necesarios para
     * que pueda funcionar correctamente
     *
     * @param modificar booleano que identificará sí el método há sido invocado desde el botón "alta"(false) o "modificar"(true).
     *                  al haber sido llamado de modificar, el método enviará variables extras a la nueva Activity.
     */
    void onclick(Boolean modificar) {
        //Declaración de la intención
        Intent i = new Intent(this, RegistroActivity.class);
        //Añadimos el boleano modificar al almacén de datos de Intent i
        i.putExtra("modificar", modificar);
        //Comprobamos si modificar es true
        if (modificar) {
            //Añadimos al almacén de datos el nombre
            i.putExtra("nombre", tvNombre.getText());
            //Añadimos al almacén de datos el apellido
            i.putExtra("apellido", tvApellido.getText());
        }
        //Iniciamos la nueva actividad y le pasamos el Intent i  y un int mayor o igual que 0 que indicará al
        //Activity desde el que se lanza que espere una respuesta cuando la otra Actividad sea finalizada
        startActivityForResult(i, 0);
    }

    /*
    Método que manejará la respuesta del Activity en caso de que el resquest code de startActivityForResult
    haya sido mayor o igual que 0
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Instanciamos la cadena que almacenará la respuesta
        String respuesta = "";
        //Comprobamos que la salida de la Actividad a la que proviene este resultCode haya
        //sido de la manera esperada
        //Al haber salido de la aplicación con el botón de atrás la condicional será false
        if (resultCode == RESULT_CANCELED) {
            //Asignamos el debido mensaje de salida abrupta de la Activity
            respuesta = getResources().getString(R.string.salida_abrupta);
            //En caso de no haber salido con el botón de atrás
        } else {
            //Removemos la etiqueta XML que cuelga del contenedorPadre en su 3ra posición
            //En este caso textView_no_contacto
            contenedorPadre.removeViewAt(3);

            //Instanciamos un LayoutInflater con el inflater de la Activity
            LayoutInflater inflater = LayoutInflater.from(this);
            //Inflamos la nueva vista y adjuntamos su xml al contenedorPadre
            inflater.inflate(R.layout.vista_contacto, contenedorPadre, true);
            //Intanciamos el textView Nombre de la nueva vista

            tvNombre = findViewById(R.id.textView_contacto_nombre);
            //Intanciamos el textView Apellido de la nueva vista
            tvApellido = findViewById(R.id.textView_contacto_apellido);

            //Asignamos el nombre al textView Nombre de la nueva vista
            tvNombre.setText(data.getExtras().getString("nombre"));
            //Asignamos el apellido al textView Apellido de la nueva vista
            tvApellido.setText(data.getExtras().getString("apellido"));

            //Comprobamos que la Activity anterior era modificar y no dar alta
            if (data.getExtras().getBoolean("modificar")) {
                //Asignamos a la cadena respuesta el mensaje de modificado correctamente
                respuesta = getResources().getString(R.string.modificado_correctamente);
            } else {
                //Asignamos a la cadena respuesta el mensaje de alta correctamente
                respuesta = getResources().getString(R.string.alta_correctamente);
                //Habilitamos el botón modificar
                botonModificar.setEnabled(true);
                //Deshabilitamos el botón dar alta
                botonAlta.setEnabled(false);
            }
        }
        //Lanzamos mensaje de respuesta al usuario
        Toast.makeText(getApplicationContext(), respuesta, Toast.LENGTH_LONG).show();
    }

}