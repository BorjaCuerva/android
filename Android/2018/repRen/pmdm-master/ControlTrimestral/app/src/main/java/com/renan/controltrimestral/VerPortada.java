package com.renan.controltrimestral;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class VerPortada extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_portada);
        int portada =getIntent().getIntExtra("portada",0);
        if (portada!=0){
            ImageView imv = findViewById(R.id.imagenPortada);
            imv.setImageResource(portada);

        }
    }
}
