package com.renan.controltrimestral;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.renan.controltrimestral.entidades.Libro;
import com.renan.controltrimestral.utilidades.Constantes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    ArrayList<String> arrayList;
    Activity activity = this;
    ArrayList<Libro> libros;
    ArrayList<Libro> modificarFav;
    ArrayList<String> autores;
    SQLiteOpenHelper con;
    Adaptador adaptador;
    ListView listView;
    boolean hayPortada = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // con = new ConectorSQLiteHelper(this, "asdasdasda", null, 1);
        con = new SQLiteOpenHelper(this, "hola", null, 1) {
            @Override
            public void onCreate(SQLiteDatabase sqLiteDatabase) {
                arrayList = llenarArrayList();
                for (String lineaSQL : arrayList) {
                    sqLiteDatabase.execSQL(lineaSQL);
                }
            }

            @Override
            public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLA_LIBROS);
                onCreate(sqLiteDatabase);
            }
        };
        libros = new ArrayList<>();

        autores = new ArrayList<>();
        modificarFav = new ArrayList<>();
        cargarAutores();
        //POBLAR SPINNER
        spinner = findViewById(R.id.spinner_autor);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, autores);
        spinner.setAdapter(arrayAdapter);
        //FIN POBLAR SPINNER;


        listView = findViewById(R.id.listalibros);
        cargarLibros();
        adaptador = new Adaptador(activity, libros);
        listView.setAdapter(adaptador);
        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Libro libro = (Libro) listView.getItemAtPosition(i);
                if (libro.getFavorito() == 0) {
                    libro.setFavorito(1);
                    modificarFav.add(libro);
                } else {
                    libro.setFavorito(0);
                    modificarFav.add(libro);
                }

                adaptador.notifyDataSetChanged();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adaptador.clear();
                String item = (String) spinner.getItemAtPosition(position);
                if (item.equals("Filtrar por autores")) {
//                    Toast.makeText(activity, "hola", Toast.LENGTH_LONG).show();
                    cargarLibros();
                    adaptador.notifyDataSetChanged();
                } else {
                    cargarLibroAutores(item);
                    adaptador.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        for (Libro libro : modificarFav
                ) {
            updateFavoritos(libro.getId(),libro.getFavorito());
        }
    modificarFav.clear();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (hayPortada) {
            Intent intent = new Intent(this, VerPortada.class);
            intent.putExtra("modificar", false);
            Libro itema = (Libro) listView.getItemAtPosition(info.position);
            try {
                Field f = R.drawable.class.getDeclaredField("p" + itema.getPortada());
                intent.putExtra("portada", f.getInt(null));
            } catch (Exception e) {
            }
            //Extraemos el número de recurso del campo antes solicitado e insertamos la imagen
            startActivityForResult(intent, 1);
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) menuInfo;
        Libro item = (Libro) listView.getItemAtPosition(info.position);
        menu.setHeaderTitle("Operaciones sobre " + (item.getTitulo()));
        inflater.inflate(R.menu.menu_contextual, menu);
        if (item.getPortada() != 0) {
            MenuItem libro = menu.findItem(R.id.ReiniciaTextOp);
            libro.setTitle("Ver Portada");
            hayPortada = true;
        } else {
            MenuItem libro = menu.findItem(R.id.ReiniciaTextOp);
            libro.setTitle("No hay Portada");
            hayPortada = false;
        }
    }


    private ArrayList<String> llenarArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        InputStream is = getResources().openRawResource(R.raw.tituloautor);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String linea = "";
        try {
            while ((linea = br.readLine()) != null) {
                arrayList.add(linea);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Devolvemos el arrayList con sus datos completados
        return arrayList;
    }

    private void cargarLibros() {
        libros.clear();
        SQLiteDatabase db = con.getReadableDatabase();
        Cursor c = db.rawQuery(Constantes.CONSULTAR_TABLA_LIBROS, null);
        while (c.moveToNext()) {
            libros.add(new Libro(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3), c.getInt(4)));
        }
    }

    private void cargarAutores() {
        SQLiteDatabase db = con.getReadableDatabase();
        Cursor c = db.rawQuery(Constantes.CONSULTAR_TABLA_LIBROS_AUTORES, null);
        autores.add("Filtrar por autores");
        while (c.moveToNext()) {
            autores.add(c.getString(0));
        }
    }

    private void updateFavoritos(int id, int fav) {
        SQLiteDatabase db = con.getWritableDatabase();
        db.execSQL("UPDATE LIBROS SET FAVORITO ='" + fav + "' WHERE _id='" + id+"'");
    }

    private void cargarLibroAutores(String autor) {
        libros.clear();
        SQLiteDatabase db = con.getReadableDatabase();
        Cursor c = db.rawQuery(Constantes.CONSULTAR_TABLA_LIBROS_AUTORES_AUTOR + "'" + autor + "'", null);
        while (c.moveToNext()) {
            libros.add(new Libro(c.getInt(0), c.getString(1), c.getString(2), c.getInt(3), c.getInt(4)));
        }
    }
}
