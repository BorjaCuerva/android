package com.renan.controltrimestral;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.renan.controltrimestral.entidades.Libro;

import java.util.ArrayList;


public class Adaptador extends ArrayAdapter<Libro> {

    Activity contexto;

    public Adaptador(Activity context, ArrayList<Libro> objects) {
        super(context, R.layout.list_item, objects);

        this.contexto = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolder holder;

        if (item == null) {

            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.titulo = item.findViewById(R.id.LblTitulo);
            holder.autor = item.findViewById(R.id.LblAutor);
            holder.icono = item.findViewById(R.id.icono);
            item.setTag(holder);

        } else {
            holder = (ViewHolder) item.getTag();

        }

        holder.titulo.setText(getItem(position).getTitulo());
        if (getItem(position).getFavorito()==0){
            holder.titulo.setTextColor(Color.parseColor("#FF0000"));
        }else{
            holder.titulo.setTextColor(Color.parseColor("#00FF00"));
        }

        holder.autor.setText(getItem(position).getAutor());


        switch (getItem(position).getPortada()) {
            case 1:
                holder.icono.setImageResource(R.drawable.ic_favorito);
                break;
            default:
                holder.icono.setImageResource(R.drawable.ic_no_favorito);
        }

        return item;
    }

    class ViewHolder {
        ImageView icono;
        //Peso del vehículo
        TextView titulo;
        //Potencia en caballos del vehículo
        TextView autor;
    }
}
