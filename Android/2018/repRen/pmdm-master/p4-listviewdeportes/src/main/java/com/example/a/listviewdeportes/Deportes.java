package com.example.a.listviewdeportes;

public class Deportes {
    private int imgFoto;
    private String titulo;
    private boolean checkBoxDeporte;


    public Deportes(int imgFoto, String titulo, boolean checkBoxDeporte) {
        this.imgFoto = imgFoto;
        this.titulo = titulo;
        this.checkBoxDeporte = checkBoxDeporte;

    }

    public void setCheckBoxDeporte(boolean checkBoxDeporte) {
        this.checkBoxDeporte = checkBoxDeporte;
    }

    public boolean isCheckBoxDeporte() {
        return checkBoxDeporte;
    }

    public int getImgFoto() {
        return imgFoto;
    }

    public String getTitulo() {
        return titulo;
    }


}
