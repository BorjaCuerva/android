package com.example.alumno.pruebaficheros;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private Button btnEscribirFichero = null;
    private Button btnLeerFichero = null;
    private Button btnLeerRaw = null;
    private TextView volcado;

    final String[] datos =new String[10];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for(int i=1; i<=10; i++)
            datos[i-1] = "Elemento"+i;

        btnEscribirFichero = (Button)findViewById(R.id.BtnEscribirFichero);
        btnLeerFichero = (Button)findViewById(R.id.BtnLeerFichero);
        btnLeerRaw = (Button)findViewById(R.id.BtnLeerRaw);

        btnEscribirFichero.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0)
            {
                try
                {
                    OutputStreamWriter fout =
                            new OutputStreamWriter(
                                    openFileOutput("prueba_int.txt", Context.MODE_PRIVATE));

                    //fout.write("Texto de prueba.");

                    for (int i=0; i<datos.length; i++){

                        fout.write(datos[i]+"\n");
                    }
                    fout.close();

                    Log.i("Ficheros", "Fichero creado!");
                    volcado=(TextView)findViewById(R.id.etiqueta1);
                    volcado.setText("Fichero creado");


                }
                catch (Exception ex)
                {
                    Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                }
            }
        });

        btnLeerFichero.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0)
            {
                try
                {
                    String Textocompleto="";
                    String Linea;
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("prueba_int.txt")));

                    Linea = fin.readLine();
                    while(Linea!=null) {
                        Textocompleto+=Linea;
                        Textocompleto+="\n";
                        Linea = fin.readLine();
                    };
                    // String texto = fin.readLine();
                    fin.close();
                    volcado=(TextView)findViewById(R.id.etiqueta1);
                    volcado.setText(Textocompleto);
                    Log.i("Ficheros", "Fichero leido!");
                    Log.i("Ficheros", "Texto: " + Linea);
                }
                catch (Exception ex)
                {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }
            }
        });

        btnLeerRaw.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0)
            {
                String linea = "";

                try
                {
                    String Textocompleto="";
                    String Linea;
                    InputStream fraw =
                            getResources().openRawResource(R.raw.prueba_raw);

                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));
                    Linea = brin.readLine();
                    while(Linea!=null) {
                        Textocompleto+=Linea;
                        Textocompleto+="\n";
                        Linea = brin.readLine();
                    };
                    fraw.close();
                    volcado=(TextView)findViewById(R.id.etiqueta1);
                    volcado.setText(Textocompleto);
                    Log.i("Ficheros", "Fichero RAW leido!");
                    Log.i("Ficheros", "Texto: " + Linea);
                }
                catch (Exception ex)
                {
                    Log.e("Ficheros", "Error al leer fichero desde recurso raw");;
                }
            }
        });
    }


}