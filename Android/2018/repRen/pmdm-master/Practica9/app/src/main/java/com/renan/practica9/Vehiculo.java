package com.renan.practica9;

public class Vehiculo {
    //Nombre de la foto del vehículo
    private String ruta;
    //Nombre del vehículo
    private String nombre;
    //Peso del vehículo
    private int peso;
    //Potencia en caballos del vehículo
    private int potenciaCv;
    //Par motor del vehículo
    private int parMotor;

    /*
    CONSTRUCTOR
     */
    public Vehiculo(String ruta, String nombre, int peso, int potenciaCv, int parMotor) {
        this.ruta = ruta;
        this.nombre = nombre;
        this.peso = peso;
        this.potenciaCv = potenciaCv;
        this.parMotor = parMotor;
    }
    /*
    FIN CONSTRUCTOR
     */


    /*
     GETTERS AND SETTERS
    */

    public String getRuta() { return ruta; }

    public void setRuta(String ruta) { this.ruta = ruta; }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getPotenciaCv() {
        return potenciaCv;
    }

    public void setPotenciaCv(int potenciaCv) {
        this.potenciaCv = potenciaCv;
    }

    public int getParMotor() {
        return parMotor;
    }

    public void setParMotor(int parMotor) {
        this.parMotor = parMotor;
    }
    /*
    FIN GETTERS AND SETTERS
     */

}
