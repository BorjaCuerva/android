package com.renan.practica9;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class VehiculoActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.EditText_menu:
                openDialog();
                return true;
            default:
                finish();
                return super.onOptionsItemSelected(item);

        }
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.acerca_de);
        dialog.setTitle(R.string.app_name);
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detalle);
     //   Toast.makeText(getApplicationContext(), "" + getActionBar().getTitle(), Toast.LENGTH_LONG).show();


        Toolbar actionBarToolbar = findViewById(R.id.action_bar);
        if (actionBarToolbar != null)
            actionBarToolbar.setTitleTextColor(getResources().getColor(R.color.titulo));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        String ruta = getIntent().getExtras().getString("ruta");
        String name = getIntent().getExtras().getString("nombre");
        int cv = getIntent().getExtras().getInt("cv");
        int peso = getIntent().getExtras().getInt("peso");
        int par = getIntent().getExtras().getInt("par");
        double cvPorKg = (double) peso / cv;
        TextView tvNombre = findViewById(R.id.textView_nombre_vehiculo);
        TextView tvPotencia = findViewById(R.id.textView_potencia_vehiculo);
        TextView tvPar = findViewById(R.id.textView_par_vehiculo);
        TextView tvPeso = findViewById(R.id.textView_peso_vehiculo);
        TextView tvRelacion = findViewById(R.id.textView_relacion_pesoPontencia_vehiculo);
        tvNombre.setText(name);
        tvPotencia.setText("" + cv + "cv");
        tvPar.setText("" + par + "nm");
        tvPeso.setText("" + peso + "Kg");
        tvRelacion.setText("" + Double.toString(cvPorKg).substring(0, 3) + " Kg/CV");

        this.setTitle(name);

                /*
        Poniedo los txtViews visibles
         */
        TextView tvPotenciaTxt = findViewById(R.id.textView_potencia_txt);
        tvPotenciaTxt.setVisibility(View.VISIBLE);

        TextView tvPesoTxt = findViewById(R.id.textView_peso_txt);
        tvPesoTxt.setVisibility(View.VISIBLE);

        TextView tvParTxt = findViewById(R.id.textView_par_txt);
        tvParTxt.setVisibility(View.VISIBLE);

        TextView tvRelacionTxt = findViewById(R.id.textView_relacion_pesoPontencia_vehiculo_txt);
        tvRelacionTxt.setVisibility(View.VISIBLE);

        ImageView iv = findViewById(R.id.imageView_imagen_vehiculo);

        try {
            Field f = R.drawable.class.getDeclaredField(ruta);
            iv.setImageResource(f.getInt(null));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
