package com.renan.practica8;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Adaptador extends ArrayAdapter<Vehiculo> {
    Activity contexto;


    public Adaptador(Activity context, ArrayList<Vehiculo> objects) {
        super(context, R.layout.fila_lista, objects);
        this.contexto = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolder holder;
        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.fila_lista, null);
            holder = new ViewHolder();
            holder.nombre = item.findViewById(R.id.textView_nombre);
            holder.fotoCoche = item.findViewById(R.id.imageView_coche);
            holder.peso = item.findViewById(R.id.textView_peso);
            holder.potenciaCv = item.findViewById(R.id.textView_potencia);
            holder.parMotor = item.findViewById(R.id.textView_par);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }
        holder.nombre.setText(getItem(position).getNombre());
        holder.peso.setText(getItem(position).getPeso()+"Kg");
        holder.potenciaCv.setText(getItem(position).getPotenciaCv()+"cv");
        holder.parMotor.setText(getItem(position).getParMotor()+"nm");


        try {
            //Nos permite el acceso dinamico a los campos y clases declaradas en esta actividad
            Field f = R.drawable.class.getDeclaredField(getItem(position).getRuta());
            //Extraemos el número de recurso del campo antes solicitado e insertamos la imagen
            holder.fotoCoche.setImageResource(f.getInt(null));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return item;
    }


    class ViewHolder {
        //Nombre del vehículo
        TextView nombre;
        ImageView fotoCoche;
        //Peso del vehículo
        TextView peso;
        //Potencia en caballos del vehículo
        TextView potenciaCv;
        //Par motor del vehículo
        TextView parMotor;
    }
}
