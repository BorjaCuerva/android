package com.renan.practica8;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Vehiculo> arrayList;
    Adaptador adaptador;
    Activity activity = this;
    static int elegirVehiculo= 0;
    ListView listaVehiculos;
    Spinner spin;
    Vehiculo v;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       // adaptador.clear();
        listaVehiculos.setAdapter(adaptador);
    }

    //Método se activa al seleccionar cualquier elemento del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.EditTextOp:
                openDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.acerca_de);
        dialog.setTitle(R.string.app_name);
        dialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        spin = findViewById(R.id.postfield_category);



        //AÑADIR LOGO AL ACTION BAR
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_car_icon);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        //CAMBIAR COLOR TITULO ACTION BAR
        Toolbar actionBarToolbar = findViewById(R.id.action_bar);
        actionBarToolbar.setTitleTextColor(getResources().getColor(R.color.titulo));

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adaptador.clear();
                switch (position) {
                    case 0:
                        adaptador = new Adaptador(activity, llenarArrayList(0));
                        elegirVehiculo=0;
                        listaVehiculos.setAdapter(adaptador);
                        break;
                    case 1:
                        adaptador = new Adaptador(activity, llenarArrayList(1));
                        elegirVehiculo=1;
                        listaVehiculos.setAdapter(adaptador);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        listaVehiculos = findViewById(R.id.ListView_vehiculos);
        adaptador = new Adaptador(this, llenarArrayList(elegirVehiculo));
        listaVehiculos.setAdapter(adaptador);

        listaVehiculos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                v = (Vehiculo) listaVehiculos.getItemAtPosition(position);
                onclick(v);
//                Toast.makeText(getApplicationContext(), "" + v.getNombre(), Toast.LENGTH_LONG).show();
            }
        });
    }

    void onclick(Vehiculo v) {
        Intent i = new Intent(this, VehiculoActivity.class);
        i.putExtra("nombre",v.getNombre());
        i.putExtra("ruta",v.getRuta());
        i.putExtra("cv",v.getPotenciaCv());
        i.putExtra("par",v.getParMotor());
        i.putExtra("peso",v.getPeso());
        //Iniciamos la nueva actividad y le pasamos el Intent i  y un int mayor o igual que 0 que indicará al
        //Activity desde el que se lanza que espere una respuesta cuando la otra Actividad sea finalizada
        startActivityForResult(i, -1);
    }

    /*
    Entorno de pruebas
     */
    private ArrayList<Vehiculo> llenarArrayList(int posSpinner) {
        arrayList = new ArrayList<>();

        InputStream is = getResources().openRawResource((posSpinner==0)?R.raw.coches:R.raw.motos);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String linea = "";
        try {
            while ((linea = br.readLine()) != null) {
                String[] array = linea.split(",");
                arrayList.add(new Vehiculo(array[0], array[1], Integer.valueOf(array[2]), Integer.valueOf(array[3]), Integer.valueOf(array[4])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Devolvemos el arrayList con sus datos completados
        return arrayList;
    }
}
