package com.renan.practica10;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.renan.practica10.entidades.Nota;
import com.renan.practica10.utilidades.Constantes;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Activity activity = this;
    ConectorSQLiteHelper con;
    ListView listaNotas;
    GridView gridNotas;
    TextView textView_categoria;
    TextView textView_titulo;
    ArrayList<Nota> notas;
    FloatingActionButton my_fab;
    Adaptador adaptador;
    LayoutInflater inflater;
    RelativeLayout linear_layout;
    Intent intent;
    private Menu menu;
    private boolean listView = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Abrimos conexión con SQLite
        con = new ConectorSQLiteHelper(getApplicationContext(), Constantes.BD_NOTAS, null, Constantes.VERSION);
        //Instanciamos el almacén de notas;
        notas = new ArrayList<>();
        //Cargamos notas (Sí no existen notas, el valor de "ArrayList<Notas> notas" seguirá siendo 0)
        cargarNotas();


        //FLOATING BUTTON

        intent = new Intent(this, RegistrarNota.class);
        my_fab = findViewById(R.id.my_fab);
        my_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                my_fab.animate().scaleX(1).scaleY(1).rotation(360).setDuration(600);
//                Toast.makeText(getApplicationContext(), "CLICK FLOAT", Toast.LENGTH_LONG).show();
                intent.putExtra("modificar", false);
                startActivityForResult(intent, 1);
            }
        });
        //FIN FLOATING BUTTON

        linear_layout = findViewById(R.id.linear_layout);
        inflater = LayoutInflater.from(activity);
        //Si el registro de notas está vacio, cargaremos el layout "lista_vacia"
        //Si el registro de notas no está vacio, cargaremos el layout "lista_notas"
        if (notas.size() == 0) {
            //Inflando "lista_vacia"
            inflater.inflate(R.layout.lista_vacia, linear_layout, true);

        } else {
            inflarListView();
            adaptador.notifyDataSetChanged();
        }

    }


    /*
    Método que se activa al volver de una otra activity
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            notas.clear();
            cargarNotas();
            if (notas.size() == 1) {
                linear_layout.removeViewAt(1);
                inflarListView();
            }
            if (notas.size() != 0) {
                menu.getItem(0).setVisible(true);
                adaptador.notifyDataSetChanged();
                linear_layout = findViewById(R.id.linear_layout);
                int x = linear_layout.getChildCount();
                if (x == 3) {
                    linear_layout.removeViewAt(1);
                }
            }
        }
    }


    //TEST
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Nota nota = (Nota) listaNotas.getItemAtPosition(info.position);
        SQLiteDatabase db = con.getReadableDatabase();
        db.delete(Constantes.TABLA_NOTAS, Constantes.CAMPO_ID + "=?", new String[]{String.valueOf(nota.getId())});
        notas.remove(nota);
        db.close();

        adaptador.notifyDataSetChanged();


        if (notas.size() == 0) {
            inflater.inflate(R.layout.lista_vacia, linear_layout, true);
            menu.getItem(0).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        if (notas.isEmpty()) {
            this.menu.getItem(0).setVisible(false);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_view:
                if (listView) {
                    listView = false;
                    menu.getItem(0).setIcon(R.drawable.ic_list_view);
                    linear_layout.removeViewAt(1);
                    inflarGridView();
                } else {
                    menu.getItem(0).setIcon(R.drawable.ic_grid_view);
                    listView = true;
                    linear_layout.removeViewAt(1);
                    inflarListView();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) menuInfo;

        if (listView) {
            if (v.getId() == R.id.listView_notas) {
                //Añadimos el titulo al menu Conextual
                menu.setHeaderTitle(((Nota) listaNotas.getItemAtPosition(info.position)).getTitulo() + " seleccionado");

                inflater.inflate(R.menu.menu_contextual, menu);
            }
        } else {
            if (v.getId() == R.id.gridView_notas) {

                //Añadimos el titulo al menu Conextual
                menu.setHeaderTitle(((Nota) gridNotas.getItemAtPosition(info.position)).getTitulo() + " seleccionado");

                inflater.inflate(R.menu.menu_contextual, menu);
            }
        }
    }

    //FIN TEST

    private void inflarGridView() {


        inflater.inflate(R.layout.grid_notas, linear_layout, true);

        gridNotas = findViewById(R.id.gridView_notas);
        //Cargando adaptador
        adaptador = new Adaptador(activity, R.layout.vista_grid_list, notas);
        //Estableciendo el adaptador al grid view;
        gridNotas.setAdapter(adaptador);
        //Registramos el menu contextual al gridView
        registerForContextMenu(gridNotas);


        gridNotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Nota item = (Nota) gridNotas.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(), ""+item.getId(), Toast.LENGTH_LONG).show();
                intent.putExtra("modificar", true);
                intent.putExtra("id", "" + item.getId());
                intent.putExtra("categoria", item.getIcono());
                intent.putExtra("titulo", item.getTitulo());
                intent.putExtra("descripcion", item.getDescripcion());
                startActivityForResult(intent, 1);
            }
        });
    }

    private void inflarListView() {
        //Inflando "lista_notas"
        inflater.inflate(R.layout.lista_notas, linear_layout, true);
        //Instanciando list_view en java
        listaNotas = findViewById(R.id.listView_notas);
        //Cargando adaptador
        adaptador = new Adaptador(activity, R.layout.vista_nota_list, notas);
        //Estableciendo el adaptador al list view;
        listaNotas.setAdapter(adaptador);
        //Registramos el menu contextual al listView
        registerForContextMenu(listaNotas);


        listaNotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Nota item = (Nota) listaNotas.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(), ""+item.getId(), Toast.LENGTH_LONG).show();
                intent.putExtra("modificar", true);
                intent.putExtra("id", "" + item.getId());
                intent.putExtra("categoria", item.getIcono());
                intent.putExtra("titulo", item.getTitulo());
                intent.putExtra("descripcion", item.getDescripcion());
                startActivityForResult(intent, 1);
            }
        });
    }


    private void cargarNotas() {
        SQLiteDatabase db = con.getReadableDatabase();
        Cursor c = db.rawQuery(Constantes.CONSULTAR_TABLA_NOTAS, null);
        while (c.moveToNext()) {
            notas.add(new Nota(c.getInt(0), c.getString(1), c.getString(2), c.getString(3), c.getInt(4)));
        }
    }
}
