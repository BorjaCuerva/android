package com.renan.examen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.renan.examen.utilidades.Constantes;

public class MainActivity extends AppCompatActivity {

    ConectorSQLiteHelper con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        con = new ConectorSQLiteHelper(getApplicationContext(), Constantes.BD_NOTAS, null, Constantes.VERSION);

    }
}
