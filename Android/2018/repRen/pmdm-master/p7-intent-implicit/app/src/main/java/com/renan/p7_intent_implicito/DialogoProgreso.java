package com.renan.p7_intent_implicito;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;

public class DialogoProgreso extends DialogFragment {

    Comunicador comunicator;
    static boolean cancelar=false;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        comunicator=(Comunicador) activity;
    }

    interface Comunicador
    {
        public void onDialogMessage(boolean message);
    }

    private int miProgreso;
    private Handler miProgresoHandler;
    private static final int PROGRESO_MAX = 100;
    private ProgressDialog miProgresoDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        miProgresoDialog = new ProgressDialog(getActivity());
        miProgreso = 0;
        miProgresoDialog.setProgress(0);
        cancelar=false;
        miProgresoDialog.setIcon(android.R.drawable.ic_dialog_info);
        miProgresoDialog.setTitle(getResources().getText(R.string.texto_enviando));
        miProgresoDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        miProgresoDialog.setMax(PROGRESO_MAX);
        miProgresoDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                getResources().getText(R.string.btn_cancelar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int boton) {
                        cancelar=true;
                        comunicator.onDialogMessage(true);
                    }
                });
        // Controlador (hilo) que simula un cambio en el progreso.
        // Es como un temporizador que usamos para dibujar la barra de progreso
        // en una ventana de diálogo.
        miProgresoHandler = new Handler() {
            @Override
            public void handleMessage(Message msj) {
                super.handleMessage(msj);
                if (miProgreso >= PROGRESO_MAX) {
                    miProgresoDialog.dismiss();
                } else {
                    miProgreso++;
                    miProgresoDialog.incrementProgressBy(1);
                    miProgresoHandler.sendEmptyMessageDelayed(0, 15);
                }
            }
        };
        miProgresoHandler.sendEmptyMessage(0);

        return miProgresoDialog;

    }

    // Cuando desaparece la ventana de Diálogo paramos el Controlador (hilo) que simula el progreso
    @Override
    public void onStop() {
        super.onStop();
        comunicator.onDialogMessage(cancelar);
        miProgresoHandler.removeCallbacksAndMessages(null);
    }


}