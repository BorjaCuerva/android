package com.example.alumno.examenfinalmydmbruceramirez;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements  DialogPaisFragment.EditNameDialogListener{

    BDCountries baseDatos;
    ArrayList<Country> paises;
    ArrayList<String> continentes;
    ListView lista;
    AdaptadorCountry adaptador;
    Spinner spinner;
    FragmentManager fm = getSupportFragmentManager();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CountriesDBHelper.ins = getResources().openRawResource(getResources().getIdentifier("worldsqliterelacional","raw", getPackageName()));

        baseDatos = new BDCountries(this);
        baseDatos.abrir();

        continentes = new ArrayList<String>();
        paises = new ArrayList<Country>();
        lista = (ListView) findViewById(R.id.listapaises);
        adaptador = new AdaptadorCountry(this, paises);
        lista.setAdapter(adaptador);
        registerForContextMenu(lista);
        cargarElementos();

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Country temp = (Country) lista.getItemAtPosition(position);
                onIniciar(temp.getName(), temp.getCode());
            }
        });

        spinner = findViewById(R.id.spinner_continentes);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, continentes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    cargarElementos();
                } else{
                    String continente = spinner.getSelectedItem().toString();
                    Toast.makeText(getApplicationContext(), continente, Toast.LENGTH_LONG).show();
                    cargarPaises(continente);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarElementos() {
        Cursor c = baseDatos.obtenerPaises();
        paises.removeAll(paises);
        continentes.removeAll(continentes);
        continentes.add("Filtar por...");
        TextView encabezado = (TextView) findViewById(R.id.label_encabezado);
        encabezado.setText(R.string.labelencabezado);

        for(int i = 0; i<c.getCount(); i++){
            c.moveToPosition(i);
            String code = c.getString(0);
            String name = c.getString(1);
            String continent = c.getString(2);
            String region = c.getString(3);
            Float surfaceArea = c.getFloat(4);
            String capital = c.getString(5);

            Country temp = new Country(code, name, continent, region, surfaceArea, capital, "country");
            paises.add(temp);
            if(!continentes.contains(continent)){
                continentes.add(continent);
            }
        }
        adaptador.notifyDataSetChanged();
    }

    private void cargarPaises(String continente) {
        Cursor c = baseDatos.obtenerPaisesPorContinente(continente);
        paises.removeAll(paises);

        TextView encabezado = (TextView) findViewById(R.id.label_encabezado);
        encabezado.setText("Países por continente");

        for(int i = 0; i<c.getCount(); i++){
            c.moveToPosition(i);
            String code = c.getString(0);
            String name = c.getString(1);
            String continent = c.getString(2);
            String region = c.getString(3);
            Float surfaceArea = c.getFloat(4);
            String capital = c.getString(5);

            Country temp = new Country(code, name, continent, region, surfaceArea, capital, "country");
            paises.add(temp);
        }
        adaptador.notifyDataSetChanged();
    }
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        if(v.getId()==R.id.listapaises){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Country temp = (Country) lista.getItemAtPosition(info.position);
            menu.setHeaderTitle("Operaciones sobre "+(temp.getName()));
            inflater.inflate(R.menu.menu_context, menu);
            menu.getItem(0);
        }
    }
    @Override
    public boolean onContextItemSelected (MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.Pais:
                Country temp = (Country) lista.getItemAtPosition(info.position);
                DialogPaisFragment dialogPaisFragment = new DialogPaisFragment();
                Bundle bundle = new Bundle();
                bundle.putString("nombre",temp.getName());
                bundle.putString("continente",temp.getContinent());
                bundle.putString("region",temp.getRegion());
                bundle.putFloat("superficie",temp.getSurfaceArea());
                Integer capi = Integer.valueOf(temp.getCapital());
                String nombreCapi = baseDatos.obtenerNombreCapital(capi);
                bundle.putString("capital",nombreCapi);
                dialogPaisFragment.setArguments(bundle);
                dialogPaisFragment.show(fm,"Datos del País");

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void onIniciar(String name, String code){
        Intent i = new Intent(this, Main2Activity.class);
        i.putExtra("name",name);
        i.putExtra("code",code);
        startActivity(i);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String name, int id2, boolean esNuevo) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
