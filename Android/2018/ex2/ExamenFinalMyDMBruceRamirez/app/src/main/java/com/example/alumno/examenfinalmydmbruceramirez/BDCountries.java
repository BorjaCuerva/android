package com.example.alumno.examenfinalmydmbruceramirez;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BDCountries {
    private Context contexto;
    private SQLiteDatabase db;
    private CountriesDBHelper bdHelper;

    public BDCountries(Context context){
        this.contexto = context;
    }

    public BDCountries abrir() throws SQLException {
        bdHelper = new CountriesDBHelper(this.contexto, "DBUsuarios", null, 1);
        db = bdHelper.getWritableDatabase();
        return null;
    }

    public void cerrar(){
        bdHelper.close();
    }

    public Cursor obtenerPaises(){
        Cursor b = db.rawQuery("SELECT Code, Name, Continent, Region, SurfaceArea, Capital FROM country;",null);
        return b;
    }

    public Cursor obtenerPaisesPorContinente(String continente){
        Cursor d = db.rawQuery("SELECT Code, Name, Continent, Region, SurfaceArea, Capital FROM country WHERE Continent = '"+continente+"';",null);
        return d;
    }

    public Cursor getCiudadPais(String countryCode){
        Cursor f = db.rawQuery("SELECT Name, CountryCode, District FROM city WHERE CountryCode = '"+countryCode+"';",null);
        return f;
    }
    public String obtenerNombreCapital(int capit){
        Cursor g = db.rawQuery("SELECT Name from city WHERE _id ="+capit+";",null);
        g.moveToPosition(0);
        String nombreCapi = g.getString(0);
        return nombreCapi;
    }
}
