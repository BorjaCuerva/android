package com.example.alumno.examenfinalmydmbruceramirez;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DialogPaisFragment extends DialogFragment {

    public interface EditNameDialogListener{
        void onDialogPositiveClick (DialogFragment dialog, String name, int id2, boolean esNuevo);
        void onDialogNegativeClick (DialogFragment dialog);
    }

    private EditText nombreEditText;
    private AlertDialog.Builder ventana;

    public DialogPaisFragment(){

    }
     EditNameDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mListener = (EditNameDialogListener) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString()+" must implement ShareDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.dialog_fragment,null);
        final TextView nombre = (TextView) v.findViewById(R.id.textViewNombre);
        final TextView continente = (TextView) v.findViewById(R.id.textViewContinente);
        final TextView region = (TextView) v.findViewById(R.id.textViewRegion);
        final TextView superficie = (TextView) v.findViewById(R.id.textViewSuperficie);
        final TextView capital = (TextView) v.findViewById(R.id.textViewCapital);
        Bundle caja = getArguments();
        nombre.setText("Nombre: "+caja.getString("nombre"));
        continente.setText("Continente: "+caja.getString("continente"));
        region.setText("Region: "+caja.getString("region"));
        superficie.setText("Superficie: "+caja.getFloat("superficie"));
        capital.setText("Capital: "+caja.getString("capital"));
        return new AlertDialog.Builder(getActivity()).setTitle("Datos del Pais").setView(v).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Has pulsado el boton 'Aceptar'",Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(),"Has pulsado el boton Cancelar",Toast.LENGTH_SHORT).show();
            }
        }).create();
    }
}
