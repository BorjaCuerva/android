package com.example.alumno.examenfinalmydmbruceramirez;

public class City {
    String name;
    String countryCode;
    String district;
    String tabla;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    public City(String name, String countryCode, String district, String tabla) {
        this.name = name;
        this.countryCode = countryCode;
        this.district = district;
        this.tabla = tabla;
    }

    public City(String name) {
        this.name = name;
    }
}
