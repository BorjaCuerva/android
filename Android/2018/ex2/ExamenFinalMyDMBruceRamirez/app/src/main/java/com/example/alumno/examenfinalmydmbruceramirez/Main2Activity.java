package com.example.alumno.examenfinalmydmbruceramirez;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {
    TextView encabezado2;
    ArrayList<City> ciudades;
    BDCountries baseDatos;
    ListView listaSecond;
    AdaptadorCity adaptadorSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final String name = getIntent().getStringExtra("name");
        final String code = getIntent().getStringExtra("code");

        encabezado2 = (TextView) findViewById(R.id.label_encabezado2_main2);
        encabezado2.setText("País: "+name);

        baseDatos = new BDCountries(this);
        baseDatos.abrir();

        ciudades = new ArrayList<City>();
        listaSecond = (ListView) findViewById(R.id.listaciudades);
        adaptadorSecond = new AdaptadorCity(this, ciudades);
        listaSecond.setAdapter(adaptadorSecond);
        cargarElementosSecond(code);

        listaSecond.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void cargarElementosSecond(String code) {
        Cursor c = baseDatos.getCiudadPais(code);
        ciudades.removeAll(ciudades);
        for(int i = 0; i<c.getCount(); i++){
            c.moveToPosition(i);
            String name = c.getString(0);
            String codeSecond = c.getString(1);
            String district = c.getString(2);

            City temp = new City(name, codeSecond, district,"city");
            ciudades.add(temp);
        }
        adaptadorSecond.notifyDataSetChanged();
    }
}
