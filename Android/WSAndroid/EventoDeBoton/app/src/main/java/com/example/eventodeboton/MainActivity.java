package com.example.eventodeboton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.ParseException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botonCalcular;
        botonCalcular = findViewById(R.id.botonCalcular);

        botonCalcular.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calcular();
                    }
                }
        );


    }

    /**
     * Metodo para calcular kilometros a millas y millas a kilometros
     */

    private void calcular() {

        String frase; //Frase que introducimos como resultado
        DecimalFormat formateador = new DecimalFormat("####.##");
        EditText introducirNumero = findViewById(R.id.introduceNumero);
        RadioButton aKilometros = findViewById(R.id.radioButtonAKilometros);
        TextView textoFinal = findViewById(R.id.resultadoFinal);
        Double numero;
        String numeroFinal;


        if (introducirNumero.getText().length() == 0) {
            Toast.makeText(this, "¡¡Introduce numero!!", Toast.LENGTH_SHORT).show(); //Mensaje que avisa al usuario para introducir numero en caso de que no lo haga
        }else{
            //Si el boton a kilometros esta seleccionado
            if (aKilometros.isChecked()) {

                numero = Double.parseDouble(introducirNumero.getText().toString()); //Guardamos el editText como Double con parseDouble
                numeroFinal = formateador.format(numero * 0.6214) ; //Formateamos el resultado para que tenga 2 decimales
                frase = "Kms son " + numeroFinal + " Millas"; //Construimos la frase a mostrar
                textoFinal.setText(frase); //Introducimos el texto en el TextView

            //Si el boton a millas esta seleccionado
            }else{

                numero = Double.parseDouble(introducirNumero.getText().toString()); //Guardamos el editText como Double con parseDouble
                numeroFinal = formateador.format(numero * 1.609344) ; //Formateamos el resultado para que tenga 2 decimales
                frase = "Kms son " + numeroFinal + " Kilometros"; //Construimos la frase a mostrar
                textoFinal.setText(frase); //Introducimos el texto en el TextView

            }
        }


    }
}
