package com.example.ciudadesdelmundo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorCountry extends ArrayAdapter<Country>{

    Activity contexto;

    public AdaptadorCountry(Activity context, ArrayList<Country> listaPaises){
        super(context, R.layout.list_item_paises, listaPaises);
        this.contexto = context;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View item = convertView;
        ViewHolder holder;

        if(item == null){
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.list_item_paises, null);
            holder = new ViewHolder();

            holder.name = (TextView) item.findViewById(R.id.nombrePais);

            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }
        Country pais = getItem(position);

        holder.name.setText(pais.getName());

        return (item);
    }

    class ViewHolder{
        TextView name;
    }
}
