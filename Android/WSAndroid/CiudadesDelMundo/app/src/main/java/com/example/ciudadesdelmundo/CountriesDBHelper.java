package com.example.ciudadesdelmundo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CountriesDBHelper extends SQLiteOpenHelper {

    public static InputStream ins;
    private static String BD_NOMBRE="bdcountries.db";
    private static final int  BD_VERSION=1;

    public CountriesDBHelper(Context context){
        super(context, BD_NOMBRE, null, BD_VERSION);
    }

    public CountriesDBHelper(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version){
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase database){
        insertar(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS city");
        database.execSQL("DROP TABLE IF EXISTS country");
        insertar(database);
    }

    private void insertar(SQLiteDatabase database) {
        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader bfr = null;
        try{
            bfr = new BufferedReader(isr);
            String linea;
            while((linea = bfr.readLine())!=null){
                database.execSQL(linea);
            }
        } catch(FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try{
                if(bfr !=null){
                    bfr.close();
                }
            } catch (Exception e2){

            }
        }
    }
}
