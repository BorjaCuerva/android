package com.example.ciudadesdelmundo;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorCity extends ArrayAdapter<City> {
    Activity contexto;

    public AdaptadorCity(Activity context, ArrayList<City> listaCiudades) {
        super(context, R.layout.list_item_ciudades, listaCiudades);
        this.contexto = context;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View item = convertView;
        ViewHolder holder;

        if(item==null){
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.list_item_ciudades, null);
            holder = new ViewHolder();

            holder.name = (TextView) item.findViewById(R.id.nombreCiudad);

            item.setTag(holder);
        } else{
            holder = (ViewHolder) item.getTag();
        }
        City ciudad = getItem(position);

        holder.name.setText(ciudad.getName());

        return (item);
    }

    class ViewHolder{
        TextView name;
    }
}
