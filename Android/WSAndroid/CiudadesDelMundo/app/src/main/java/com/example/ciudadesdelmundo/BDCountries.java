package com.example.ciudadesdelmundo;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BDCountries {
    private Context contexto;
    private SQLiteDatabase db;
    private CountriesDBHelper bdHelper;

    public BDCountries(Context context){
        this.contexto = context;
    }

    public BDCountries abrir() throws SQLException {
        bdHelper = new CountriesDBHelper(this.contexto, "DBUsuarios", null, 1);
        db = bdHelper.getWritableDatabase();
        return null;
    }

    public void cerrar(){
        bdHelper.close();
    }

    public Cursor obtenerPaises(){
        Cursor b = db.rawQuery("SELECT Code, Name, Continent, Region, SurfaceArea, Capital FROM country;",null);
        return b;
    }
    public Cursor obtenerCiudades(){
        Cursor b = db.rawQuery("SELECT Name FROM city;",null);
        return b;
    }

    public Cursor obtenerCiudad(String comienza){
        Cursor a = db.rawQuery("SELECT Name FROM city WHERE Name = "+comienza,null);
        return a;
    }

}
