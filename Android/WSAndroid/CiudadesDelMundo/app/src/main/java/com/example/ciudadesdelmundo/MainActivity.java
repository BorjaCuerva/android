package com.example.ciudadesdelmundo;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    BDCountries baseDatos;
    ArrayList<City> ciudades;
    ListView lista;
    EditText comienzaPor;
    Activity contexto = this;
    AdaptadorCountry adaptador;
    AdaptadorCity adaptadorSecond;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botonBuscar = (Button)findViewById(R.id.botonBuscar);
        lista = (ListView) findViewById(R.id.listapaises);
        comienzaPor = (EditText) findViewById(R.id.editTextComienzaPor);

        CountriesDBHelper.ins = getResources().openRawResource(getResources().getIdentifier("worldsqliterelacional","raw", getPackageName()));

        baseDatos = new BDCountries(this);
        baseDatos.abrir();

        ciudades = new ArrayList<City>();
        lista = (ListView) findViewById(R.id.listapaises);
        adaptadorSecond = new AdaptadorCity(this, ciudades);
        lista.setAdapter(adaptador);
        registerForContextMenu(lista);
        String code = comienzaPor.getText().toString();
        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cargarElementosSecond();o
            }
        });

    }

    private void cargarElementosSecond() {
        Cursor c = baseDatos.obtenerCiudades();
        ciudades.removeAll(ciudades);
        for(int i = 0; i<c.getCount(); i++){
            c.moveToPosition(i);
            String name = c.getString(0);
            City temp = new City(name,"city");
            ciudades.add(temp);
        }

        adaptadorSecond.notifyDataSetChanged();
    }




    }