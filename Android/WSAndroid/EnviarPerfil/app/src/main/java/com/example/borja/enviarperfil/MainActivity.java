package com.example.borja.enviarperfil;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imagen;
    private Button botonSeleccionar;
    private Button botonEnviar;
    private static int RESULT_LOAD_IMAGE = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * Le aniadimos al ImageView la imagen.
         */


        /**
         * Declaramos los dos botones
         */
        botonSeleccionar = (Button) findViewById(R.id.buttonSeleccionar);
        botonEnviar = (Button) findViewById(R.id.buttonEnviar);

        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getSupportFragmentManager();
                DialogoProgreso editDialog = new DialogoProgreso();
                editDialog.show(fm,null);

            }
            
        });


        /**
         * Cuando se pulsa el boton seleccionar
         */
        botonSeleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentImagen = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentImagen,RESULT_LOAD_IMAGE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String [] filePathColum = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,filePathColum,null,null,null);
            cursor.moveToFirst();

            int columIndex = cursor.getColumnIndex(filePathColum[0]);
            String picturePath = cursor.getString(columIndex);
            cursor.close();

            //Para mostrar la imagen
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            Uri uri = data.getData();
            imageView.setImageURI(uri);

        }
    }



}
