package com.example.borja.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Declaramos dos variables de tipo Button
         */
        Button botonAlta= (Button) findViewById(R.id.botonAlta); //Boton activado por defecto
        //Al inicio de la aplicacion, el boton modificar estara deshabilitado
        Button botonModificar= (Button) findViewById(R.id.botonModificar);

        //Si se pulsa el botón de Alta
        botonAlta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * LLamamos a la actividad secundaria y le mandamos el mensaje de opcion
                 */
                String resp="alta"; //String respuesta
                Intent i = new Intent(MainActivity.this,Main2Activity.class);
                i.putExtra("opcion", resp);
                startActivityForResult(i,SECONDARY_ACTIVITY_TAG);
            }
        });

        /**
         * Si se pulsa el boton modificar
         * hacemos un setOnClickListener
         */
        botonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * LLamamos a la actividad secundaria y le mandamos el mensaje de opcion
                 */
                TextView textView1 = (TextView) findViewById(R.id.textoSinContactos);
                TextView textView2 = (TextView) findViewById(R.id.textViewApellidos);
                String resp="modificacion";
                String nombre = textView1.getText().toString(); //Guardamos el nombre introducido
                String apellidos = textView2.getText().toString(); //Guardamos el apellido introducido
                Intent i = new Intent(MainActivity.this,Main2Activity.class);
                /**
                 * Hacemos los putExtra
                 */
                i.putExtra("opcion", resp);
                i.putExtra("nombre", nombre);
                i.putExtra("apellidos", apellidos);
                startActivityForResult(i,SECONDARY_ACTIVITY_TAG);
            }
        });
    }

    /**
     * Método invocado por Android cuando una de las actividades
     * que hemos lanzado desde ésta usando startActivityForResult()
     * termina.
     *
     * @param requestCode Valor del segundo parámetro que pasamos a
     * startActivityForResult() para lanzar la actividad que ahora
     * termina.
     * @param resultCode Código indicando el éxito o fracaso de la
     * actividad que ha terminado (RESULT_CANCELED o RESULT_OK).
     * @param data Intent con los datos devueltos por la actividad que
     * ha terminado (llamando a setResult()).
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String respuesta="";

        //Si se cancela la operación de la actividad secundaria mostraremos un toast
        if (resultCode == RESULT_CANCELED){
            respuesta = "Has salido de la subactividad sin pulsar el botón 'Aceptar'."; //Mensaje del toast
            Toast toast1 = Toast.makeText(getApplicationContext(),respuesta, Toast.LENGTH_SHORT);
            toast1.show(); //Mostramos el toast
        }
        else {
            //Si se sale pulsando el botón de aceptar de la actividad secundaria mostraremos un mensaje informativo según el estado de la actividad principal
            // Guardarmos los datos y cambiamos el estado de la actividad
            if(data.getStringExtra("opcion").equals("alta")) {
                Toast toast1 = Toast.makeText(getApplicationContext(), "Has dado de alta al contacto correctamente.", Toast.LENGTH_SHORT);
                toast1.show(); //Mostramos el toast
            }else
            {
                Toast toast1 = Toast.makeText(getApplicationContext(), "Modificación del contacto hecha correctamente.", Toast.LENGTH_SHORT);
                toast1.show(); //Mostramos el toast
            }
            respuesta = data.getStringExtra("vuelta");
            TextView tv = (TextView) findViewById(R.id.textoSinContactos);
            TextView tv2 = (TextView) findViewById(R.id.textViewApellidos);
            Button botonAlta= (Button) findViewById(R.id.botonAlta);
            Button botonModificar= (Button) findViewById(R.id.botonModificar);

            /**
             * Creamos un array donde meteremos el resultado final
             * Lo cortamos con un split para separar el nombre de los apellidos
             */
            String [] resultadoFinal = respuesta.split(";"); //Cortamos con split
            tv.setText("Nombre: "+resultadoFinal[0]); //nombre
            tv2.setText("Apellidos: "+resultadoFinal[1]); //apellidos
            botonAlta.setEnabled(false); //Desactivamos el boton de alta
            botonModificar.setEnabled(true); //Activamos el boton modificar
        }
    }
    private static final int SECONDARY_ACTIVITY_TAG = 1;
}