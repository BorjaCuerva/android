package com.example.borja.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_secundaria);

        Button botonA= (Button) findViewById(R.id.buttonAceptarSec); //Creamos un boton

        //Se actuará según el String que reciba opcion
        String opcion = getIntent().getStringExtra("opcion");

        /**
         * Creamos un switch para las dos posibles opciones
         * alta ---> Creamos un nuevo usuario
         * modificar ---> Modificamos un usuario existente. Esta opcion esta desactivada has ta que se cree un usuario
         */

        switch (opcion){
            //Caso para el estado alta de la actividad principal.
            case "alta":
                //al pulsar el boton
                botonA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Creamos dos editText
                        EditText e1 = (EditText) findViewById(R.id.editTextNombre);
                        EditText e2 = (EditText) findViewById(R.id.editTextApellidos);
                        /**
                         * Si alguna de las cajas de texto esta vacia
                         * Mandamos un toast con la informacion
                         */
                        if(e1.getText().toString().equals("") || e2.getText().toString().equals("")){
                            Toast toast1 = Toast.makeText(getApplicationContext(),"No puedes dejar ningún campo vacío.", Toast.LENGTH_SHORT);
                            toast1.show(); //Mostramos el toast
                        }
                        //Si ninguna esta vacia, se devolverán a la actividad principal los valores en las cajas de texto y se cerrara esta actividad.
                        else{
                            String vuelta="";
                            String opcion="alta";
                            vuelta=e1.getText().toString()+";"+e2.getText().toString(); //Informacion de vuelta unida por ; la separaremos con un split
                            Intent datos = new Intent();
                            // Metemos como datos extra del intent la respuesta del usuario
                            datos.putExtra("vuelta", vuelta);
                            datos.putExtra("opcion", opcion);
                            setResult(RESULT_OK, datos);
                            finish(); //Fin de la actividad
                        }
                    }
                });
                break;
            //Caso para el estado modificar de la actividad principal.
            case "modificacion":
                //Creamos dos editText, en los que almacenaremos la informacion del nombre y apellido
                EditText e1 = (EditText) findViewById(R.id.editTextNombre);
                EditText e2 = (EditText) findViewById(R.id.editTextApellidos);
                //Creamos un textView para cambiar el titulo de la actividad
                TextView textoSuperior = findViewById(R.id.textoSuperiorSec);
                textoSuperior.setText("Modificar contacto. Cambia el nombre y los apellidos"); //Cambiamos el texto de la actividad
                String nombre;
                String apellido;
                /**
                 * Creamos un array donde meteremos el resultado final
                 * Lo cortamos con un split para separar el nombre de los apellidos
                 */
                String [] resultadoFinal = getIntent().getStringExtra("nombre").split(": ");
                nombre=resultadoFinal[1];
                resultadoFinal = getIntent().getStringExtra("apellidos").split(": ");
                apellido=resultadoFinal[1];
                e1.setText(nombre); //Ponemos el nombre
                e2.setText(apellido); //Ponemos el apellido
                //Si se pulsa el botón
                botonA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText e1 = (EditText) findViewById(R.id.editTextNombre);
                        EditText e2 = (EditText) findViewById(R.id.editTextApellidos);
                        /**
                         * Si alguna de las cajas de texto esta vacia
                         * Mandamos un toast con la informacion
                         */
                        if(e1.getText().toString().equals("") || e2.getText().toString().equals("")){
                            Toast toast1 = Toast.makeText(getApplicationContext(),"No puedes dejar ningún campo vacío.", Toast.LENGTH_SHORT);
                            toast1.show(); //mostramos el toast
                        }
                        //Si ninguna esta vacía, se devolverán a la actividad principal los valores en las cajas de texto y se cerrará esta actividad
                        else{
                            String vuelta="";
                            String opcion="mod";
                            vuelta=e1.getText().toString()+";"+e2.getText().toString();
                            Intent datos = new Intent();
                            // Metemos como datos extra del intent la respuesta del usuario
                            datos.putExtra("vuelta", vuelta);
                            datos.putExtra("opcion",opcion);
                            setResult(RESULT_OK, datos);
                            finish(); //Fin de la actividad
                        }
                    }
                });
                break;
        }
    }

}