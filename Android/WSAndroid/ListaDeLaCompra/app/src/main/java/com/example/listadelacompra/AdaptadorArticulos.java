package com.example.listadelacompra;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * ArrayAdapter de la clase Articulo
 */
public class AdaptadorArticulos extends ArrayAdapter<Articulo> {

    Activity contexto;
    //ArrayList de los articulos
    private ArrayList<Articulo> arrayListArticulos =new ArrayList<Articulo>();

    // Contructor del adaptador usando el contexto de la aplicacion actual
    AdaptadorArticulos(Activity contexto, ArrayList<Articulo> datos) {

        // Llamamos arrayListArticulos constructor de la clase superior
        //se le pasa el xml que genera la fila y el array de objetos
        super(contexto,R.layout.listitem_articulo, datos);
        this.contexto = contexto;
        arrayListArticulos = datos;
    }

    // Metodo que dibuja la Vista de cada Opcion
// Se invoca cada vez que haya que mostrar un elemento de la lista.
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.listitem_articulo, null);

        //Mediante getItem cargamos cada uno de los objetos del array
        Articulo mielemento = getItem(position);

        TextView textView = (TextView) item.findViewById(R.id.tvn);
        textView.setText(mielemento.getNombre());
        if (mielemento.isComprado())
        {
            textView.setPaintFlags(textView.getPaintFlags() |
                    Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(Color.parseColor("#00FF00"));
        }
        else
        {
            textView.setPaintFlags(textView.getPaintFlags()
                    & ~Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(Color.parseColor("#FF0000"));
        }
        return (item);
    }

}