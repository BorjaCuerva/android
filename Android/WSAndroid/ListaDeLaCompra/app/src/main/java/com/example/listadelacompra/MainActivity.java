package com.example.listadelacompra;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

/**
 * Extiende de AppCompatActivity
 * implementa EditFragment.EditNameDialogListener
 */
public class MainActivity extends AppCompatActivity implements EditFragment.EditNameDialogListener{

    //ArrayList de articulos
    private ArrayList<Articulo> al = new ArrayList<Articulo>();
    //Adaptador de los articulos
    private AdaptadorArticulos adaptador = null;
    private TextView textView;
    FragmentManager fm = getSupportFragmentManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.tvn);

        //Aniadimos los articulos
        Articulo a1 = new Articulo("Pan", false);
        al.add(a1);
        Articulo a2 = new Articulo("Leche", false);
        al.add(a2);
        Articulo a3 = new Articulo("Periódico", false);
        al.add(a3);
        Articulo a4 = new Articulo("Fruta", false);
        al.add(a4);
        Articulo a5 = new Articulo("Carne", false);
        al.add(a5);
        Articulo a6 = new Articulo("Tomates", false);
        al.add(a6);



        adaptador = new AdaptadorArticulos(this, al);
        final ListView listaOpciones = (ListView) findViewById(R.id.ListaOpciones);
        listaOpciones.setAdapter(adaptador);
        /**
         * OnclicListener
         */
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                //Si el articulo seleccionado esta comprado, lo ponemos en false como no comprado
                if (al.get(position).isComprado()) {
                    al.get(position).setComprado(false);
                    //Ponemos el articulo como comprado y sacamos el toast
                } else {
                    al.get(position).setComprado(true);
                    Toast.makeText(v.getContext(), "Has comprado " + al.get(position).getNombre(), Toast.LENGTH_SHORT).show();
                }
                //Volvemos a poner la lista modificada
                listaOpciones.setAdapter(adaptador);
                //Modifica los cambios
                adaptador.notifyDataSetChanged();

            }

        });

        registerForContextMenu(listaOpciones);

    }

    /**
     * Metodo para crear el menu con el icono
     * @param menu
     * @return
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        //Alternativa 1
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    /**
     * A la hora de pulsar aceptar
     * @param dialog
     * @param name
     * @param position
     * @param lugar
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String name, int position, int lugar) {
        if (position == 1) {
            al.get(lugar).setNombre(name);
            adaptador.notifyDataSetChanged();
        } else {
            Articulo a = new Articulo(name, false);
            al.add(a);
            adaptador.notifyDataSetChanged();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    /**
     * Al dejar pulsado un articulo
     * @param menu
     * @param v
     * @param menuInfo
     */
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        final ListView listaOpciones = (ListView) findViewById(R.id.ListaOpciones);
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater = getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos el fichero XML correspondiente
        if (v.getId() == R.id.ListaOpciones) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            // Definimos la cabecera del menú contextual
            menu.setHeaderTitle("Operaciones sobre " + al.get(info.position).getNombre());
            inflater.inflate(R.menu.menu_context, menu);
        }
    }

    /**
     * Menu contextual editar y borrar articulo
     * editar --> edit.menu
     * borrar -->borrarMenu
     * @param item
     * @return
     */
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {

            // Se selecciona la opción Editar articulo
            case R.id.editMenu:

                String nombreaeditar = al.get(info.position).getNombre();

                EditFragment editarDFragment = new EditFragment();
                Bundle bundle = new Bundle(2);
                bundle.putString("nombreaeditar", nombreaeditar);

                bundle.putInt("posicion", 1);
                bundle.putInt("lugar",info.position);
                editarDFragment.setArguments(bundle);
                editarDFragment.show(fm, "Edita articulo");

                //Reiniciamos el adaptador para que recargue la lista
                adaptador.notifyDataSetChanged();
                return true;
            case R.id.borrarMenu:

                al.remove(info.position);
                adaptador.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.menuOp1:
                EditFragment editdFragment = new EditFragment();


                Bundle bundle = new Bundle(2);
                bundle.putInt("posicion", 2);

                editdFragment.setArguments(bundle);
                editdFragment.show(fm, "Edit Dialog Fragment");
                return true;

        }
        return false;

    }





}
