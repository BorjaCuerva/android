package com.example.listadelacompra;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;


public class EditFragment extends DialogFragment {

    public interface EditNameDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String name,int position,int lugar);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    private EditText passEditText;
    EditNameDialogListener mListener;

    private AlertDialog.Builder ventana;

    public EditFragment() {
        // Empty constructor required for DialogFragment
    }
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ( EditNameDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ShareDialogListener");
        }
    }

    //private Handler mResponseHandler;
    @Override


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String elementoeditar;
        final int posicionaeditar;
        posicionaeditar=getArguments().getInt("posicion");
        final int lugar;
        lugar=getArguments().getInt("lugar");

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.dialog_aniadir,null);
        passEditText= (EditText) v.findViewById(R.id.nombreEdit);

        if (posicionaeditar==1){
            elementoeditar= getArguments().getString("nombreaeditar");

            passEditText.setText(elementoeditar);
            passEditText.setSelectAllOnFocus(true);

            return new AlertDialog.Builder(getActivity())
                    // Set Dialog Title
                    .setTitle("Modifica Artículo")
                    //Set XML file
                    .setView(v)
                    // Set Dialog Message
                    .setMessage("Modifica el artículo")
                    // Al pulsar en aceptar
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String name=passEditText.getText().toString();
                            mListener.onDialogPositiveClick(EditFragment.this,name,1,lugar);
                        }
                    })

                    // Al pulsar el boton cancelar
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,    int which) {
                            Toast.makeText(getActivity(), "No ha modificado el producto " +passEditText.getText().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }).create();
        }
        else
        {
            /**
             * Alta de articulo
             */
            return new AlertDialog.Builder(getActivity())
                    // Set Dialog Title
                    .setTitle("Alta de Artículo")
                    //Set XML file
                    .setView(v)
                    // Set Dialog Message
                    .setMessage("Introduce un nuevo artículo")

                    // Al pulsar aceptar
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String name=passEditText.getText().toString();
                            mListener.onDialogPositiveClick(EditFragment.this,name,2,lugar);
                        }
                    })

                    // Al pulsar cancelar
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,    int which) {
                            Toast.makeText(getActivity(), "No ha añadido el  producto "+ passEditText.getText().toString()+"\n a su lista de compra", Toast.LENGTH_SHORT).show();
                        }
                    }).create();
        }
    }


}