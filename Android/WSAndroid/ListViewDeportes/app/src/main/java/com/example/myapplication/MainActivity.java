package com.example.myapplication;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    private ArrayList<Opcion> al = new ArrayList<Opcion>();
    private String llave="key";
    private AdaptadorOpciones adaptador=null;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            al = savedInstanceState.getParcelableArrayList("key");
            adaptador = new AdaptadorOpciones(this, al);
            final ListView listaOpciones = (ListView) findViewById(R.id.ListaOpciones);
            listaOpciones.setAdapter(adaptador);
        } else {


            Opcion o1 = new Opcion(getResources().getString(R.string.atl), R.drawable.atletismo, false);
            al.add(o1);
            Opcion o2 = new Opcion(getResources().getString(R.string.basket), R.drawable.baloncesto, false);
            al.add(o2);
            Opcion o3 = new Opcion(getResources().getString(R.string.futbol), R.drawable.futbol, false);
            al.add(o3);
            Opcion o4 = new Opcion(getResources().getString(R.string.golf), R.drawable.golf, false);
            al.add(o4);
            Opcion o5 = new Opcion(getResources().getString(R.string.motociclismo), R.drawable.motociclismo, false);
            al.add(o5);
            Opcion o6 = new Opcion(getResources().getString(R.string.natacion), R.drawable.natacion, false);
            al.add(o6);
            Opcion o7 = new Opcion(getResources().getString(R.string.pingpong), R.drawable.pingpong, false);
            al.add(o7);
            adaptador = new AdaptadorOpciones(this, al);
            final ListView listaOpciones = (ListView) findViewById(R.id.ListaOpciones);
            listaOpciones.setAdapter(adaptador);

            listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                    if (al.get(position).getCheckbox() == false) {
                        al.get(position).setCheckbox(true);
                    } else {
                        al.get(position).setCheckbox(false);
                    }
                    System.out.println(al.get(position).getCheckbox());
                    adaptador.notifyDataSetChanged();

                }

            });

        }


    }

    public void pulsar(View v) {
        int i;
        int j=0;
        String frase = getResources().getString(R.string.inicio);

        for (i = 0; i < al.size(); i++) {

            if (al.get(i).getCheckbox() == true) {
                if (j==0)
                {
                    frase = frase+" " + getResources().getStringArray(R.array.deportes)[i];
                    j++;
                }
                else
                {
                    frase = frase + ", " + getResources().getStringArray(R.array.deportes)[i];
                }

            }
        }
        frase=frase+".";
        Toast.makeText(v.getContext(), frase, Toast.LENGTH_SHORT).show();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelableArrayList("key", (ArrayList<? extends Parcelable>) al);
        super.onSaveInstanceState(savedInstanceState);
    }



}