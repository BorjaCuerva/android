package com.example.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Opcion implements Parcelable {

    private String nombre;
    private int icono;
    private boolean checkbox;

    public Opcion(String nombre, int icono, boolean checkbox) {
        this.nombre = nombre;
        this.icono = icono;
        this.checkbox = checkbox;
    }

    protected Opcion(Parcel in) {
        nombre = in.readString();
        icono = in.readInt();
        checkbox = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Opcion> CREATOR = new Creator<Opcion>() {
        @Override
        public Opcion createFromParcel(Parcel in) {
            return new Opcion(in);
        }

        @Override
        public Opcion[] newArray(int size) {
            return new Opcion[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public String getNombre() {
        return this.nombre;
    }


    public int getIcono() {
        return this.icono;
    }

    public boolean getCheckbox() {
        return this.checkbox;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void serIcono(int icono) {
        this.icono = icono;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeInt(icono);
        dest.writeByte((byte) (checkbox ? 1 : 0));
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }
}