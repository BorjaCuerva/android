package com.example.adivinanumero;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView textViewArriba;
    private TextView textViewAbajo;
    private EditText numeroIntroducido;
    private Button botonProbar;
    private Button botonJugarDeNuevo;
    private int numeroRandom;
    private int numeroIntentos;

    private final String STATE_ATTEMPS="numAttemps";
    private final String NUM_INTENTOS = "numeroRandom";
    private final String STATE_INTRODUCE="miNum";
    private String s;
    private int n;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (savedInstanceState !=null)//PREGUNTO SI EL PROGRAMA EJECUTADO HA ESTADO EN MEMORIA O NO, SI ESTA EN MEMORIA
        {
            numeroRandom = savedInstanceState.getInt(NUM_INTENTOS);//RECUPERO EL VALOR DEL NUMERO GENERADO CON RANDOM
            numeroIntentos = savedInstanceState.getInt(STATE_ATTEMPS);//RECUPERO EL VALOR DEL NUMERO DE INTENTOS
            n=savedInstanceState.getInt(STATE_INTRODUCE);//RECUPERO EL VALOR DEL NUMERO INTRODUCIDO POR TECLADO
            this.numeroIntroducido = findViewById(R.id.et1);//INICIALIZO VARIABLES
            numeroIntroducido.setText(String.valueOf(n));//ASIGNO MI VALOR n AL EDIT TEXT, HAGO UN CAST PARA CAMBIARLO DE INT A STRING
            this.botonProbar = findViewById(R.id.bt1); //BOTON PROBAR
            this.botonJugarDeNuevo = findViewById(R.id.buttonJugarNuevo); //BOTON JUGAR DE NUEVO
            this.textViewArriba = findViewById(R.id.textViewArriba);
            this.textViewAbajo = findViewById(R.id.tv2);
            if(n==0)//SI n ES 0, EL PROGRAMA SE INICIA DESDE 0, O SE HA JUGADO Y SE HA DECIDIDO JUGAR DE NUEVO.
            {
                reset(); //SI ES JUGAR DE NUEVO, PARA EVITAR QUE LOS INTENTOS SALGAN A 0, Y EL MENSAJE DE BIENVENIDA SE MODIFIQUE POR EL MENSAJE DE ETIQUETA MENOR/ETIQUETAMAYOR
            }//REINICIALIZAMOS JUEGO
            else//SI n ES DIFERENTE DE 0, QUIERE DECIR QUE HEMOS GIRADO LA PANTALLA EN MITAD DEL JUEGO, POR LO QUE RECUPERAMOS VALORES
            {
                if (n > this.numeroRandom)
                {
                    textViewArriba.setText(getEtiquetaMenor());
                    textViewAbajo.setText(getEtiquetatv2());
                    numeroIntroducido.setText("");
                }
                else
                {
                    if (n < this.numeroRandom)
                    {
                        textViewArriba.setText(getEtiquetaMayor());
                        textViewAbajo.setText(getEtiquetatv2());
                        numeroIntroducido.setText("");
                    }
                    else
                    {
                        textViewArriba.setText(getEtiquetaGanador());
                        numeroIntroducido.setText("");
                        botonProbar.setVisibility(View.INVISIBLE);
                        botonJugarDeNuevo.setVisibility(View.VISIBLE);
                        botonJugarDeNuevo.setText(getEtiquetaJugarNuevo());
                    }
                }
            }
        }
        else//QUIERE DECIR QUE NO HA ESTADO EN MEMORIA, ASIQUE SI GIRAMOS LA PANTALLA ES COMO SI ACABASEMOS DE INICIAR
        {
            reset();
        }
    }

    public void reiniciar(View v)//FUNCION ONCLICK DEL BOTON JUGAR DE NUEVO
    {
        reset();
    }

    public void preguntar(View v)//FUNCION ONCLICK DEL BOTON PROBAR SUERTE (botonProbar)
    {


        if(numeroIntroducido.getText().toString().equals(""))//COMPARO EL NUMEO INTRODUCIDO POR TECLADO PARA COMPROBAR SI ES 0 O NO
        {
            Toast.makeText(v.getContext(),getEtiquetaError(), Toast.LENGTH_SHORT).show();//SI ES 0 LANZO TOAST(MENSAJE ERROR)
        }
        else//SI NO ES 0 COMPRUEBO SI ES MAYOR, MENOR O IGUAL
        {
            if (Integer.parseInt(numeroIntroducido.getText().toString()) > this.numeroRandom)//MI NUMERO ES MAYOR
            {
                n=Integer.parseInt(numeroIntroducido.getText().toString());//VARIABLE AUX PARA ALMACENAR EL VALOR INTRODUCIDO PARA RECUPERARLO SI GIRAMOS PANTALLA
                textViewArriba.setText(getEtiquetaMenor());
                numeroIntentos++;//VARIABLE QUE MIDE EL NUMERO DE INTENTOS
                textViewAbajo.setText(getEtiquetatv2());
                numeroIntroducido.setText("");//TRAS PULSAR BOTON PROBAR SUERTE, PONGO VALOR VACIO PARA HACER MAS COMODO EL JUEGO
            }
            else
            {
                if (Integer.parseInt(numeroIntroducido.getText().toString()) < this.numeroRandom)//MI NUMERPO ES MENOR
                {
                    n=Integer.parseInt(numeroIntroducido.getText().toString());
                    textViewArriba.setText(getEtiquetaMayor());
                    numeroIntentos++;
                    textViewAbajo.setText(getEtiquetatv2());
                    numeroIntroducido.setText("");
                }
                else//MI NUMERO ES IGUAL
                {
                    n=Integer.parseInt(numeroIntroducido.getText().toString());
                    textViewArriba.setText(getEtiquetaGanador());
                    numeroIntroducido.setText("");
                    botonProbar.setVisibility(View.INVISIBLE);//OCULTO BOTTON PROBAR SUERTE
                    botonJugarDeNuevo.setVisibility(View.VISIBLE);//HAGO VISIBLE BOTON JUGAR DE NUEVO
                    botonJugarDeNuevo.setText(getEtiquetaJugarNuevo());
                }
            }
        }
    }
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Guardamos VALORES QUE INTERESA
        savedInstanceState.putInt(STATE_ATTEMPS, numeroIntentos);//GUARDO EL VALOR DEL NUMERO DE INTENTOS
        savedInstanceState.putInt(NUM_INTENTOS, numeroRandom);//GUARDO EL NUMERO GENERADO CON EL MATH RANDOM
        savedInstanceState.putInt(STATE_INTRODUCE,n);//GUARDO EL NUMERO INTRODUCIDO EN LA VARIABLE E

        // Llamamos al método de la superclase.
        super.onSaveInstanceState(savedInstanceState);
    }
    //FUNCION PARA REINICIALIZAR EL PROGRAMA SI QUEREMOS JUGAR DE NUEVO
    public void reset()
    {
        this.numeroRandom = (int) ((Math.random()*100)+1);//GENERO NUMERO RANDOM
        numeroIntentos =0;
        this.textViewArriba = findViewById(R.id.textViewArriba);
        textViewArriba.setText(R.string.bienvenido);//INICIO EL TEXT VIEW Y LANZO EL MENSAJE DE BIENVENIDA
        this.textViewAbajo = findViewById(R.id.tv2);
        textViewAbajo.setText(R.string.iniciacion);//INICIO EL TEXT VIEW Y LANZO MENSAJE DE RETO
        this.numeroIntroducido = findViewById(R.id.et1);//EDIT TEXT
        this.botonProbar = findViewById(R.id.bt1);//INICIALIZO E,B,B1 PARA QUE HAGA REFERENCIA A LOS CAMPOS DEL XML
        this.botonJugarDeNuevo = findViewById(R.id.buttonJugarNuevo);//BUTTON1
        botonJugarDeNuevo.setVisibility(View.INVISIBLE);//EL BOTON B1 HACE REFERENCIA AL BOTON QUE LANZO PARA JGUAR DE NUEVO AL ADIVINAR EL NUMERO
        botonProbar.setVisibility(View.VISIBLE);//hAGO REAPARECER EL BOTON DE PROBAR SUERTE
        this.s="";
        n=0;
    }


    private String getEtiquetatv2()//lanza el mensaje para controlar el singular y el plural del numero de intentos
    {
        android.content.res.Resources res = getResources();
        String numAttemps;
        numAttemps = res.getQuantityString(R.plurals.numErrores, numeroIntentos, numeroIntentos);

        return numAttemps;
    }
    private String getEtiquetaGanador()//lanza el mensaje de que se ha adivinado el numero
    {
        android.content.res.Resources res = getResources();
        String ganador;
        ganador=res.getString(R.string.ganar, numeroIntentos);
        return ganador;
    }
    public String getEtiquetaMayor()//este metodo lanza el mensaje de que el numero que debe introducir es mayor
    {
        android.content.res.Resources res = getResources();
        String mayor;
        mayor=res.getString(R.string.mayor,Integer.parseInt(numeroIntroducido.getText().toString()));
        return mayor;
    }
    public String getEtiquetaMenor()//este metodo lanza el mensaje de que el numero que debe introducir es menor
    {
        android.content.res.Resources res = getResources();
        String menor;
        menor=res.getString(R.string.menor,Integer.parseInt(numeroIntroducido.getText().toString()));
        return menor;
    }
    public String getEtiquetaError()//este metodo lanza el mensaje de erro del toast
    {
        android.content.res.Resources res = getResources();
        String error;
        error=res.getString(R.string.error);
        return error;
    }
    public String getEtiquetaJugarNuevo()//esta metodo hace referencia al segundo boton invisible que aparece cuando se adivina el juego
    {
        android.content.res.Resources res = getResources();
        String deNuevo;
        deNuevo=res.getString(R.string.jugarNuevo);
        return deNuevo;
    }


}
