package com.example.catalogotablet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

public class FragmentDetalles extends Fragment {
    private ImageView imagen;

    public FragmentDetalles() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_detalle, container, false);
    }

    public void mostrarDetalle(int imagenFinal) {
        imagen = (ImageView) getView().findViewById(R.id.imageView2);
        imagen.setImageResource(imagenFinal);
    }

}