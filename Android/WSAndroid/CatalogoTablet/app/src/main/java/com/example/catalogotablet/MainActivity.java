package com.example.catalogotablet;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements FragmentListado.FMarinaListener {

    private final int IMAGEN_GRANDE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setLogo(R.drawable.icono_delfin);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FragmentListado frgListado =
                (FragmentListado) getSupportFragmentManager()
                        .findFragmentById(R.id.FrgListado);

        frgListado.setFMarinaListener(this);
    }

    @Override
    public void onFMarinaSeleccionado(FMarina fm) {

        boolean hayFotoEspecie =
                (getSupportFragmentManager().findFragmentById(R.id.FrgDetalle) != null);

        if(hayFotoEspecie) {
            int imagen = fm.getRef();

            ((FragmentDetalles)getSupportFragmentManager()
                    .findFragmentById(R.id.FrgDetalle)).mostrarDetalle(imagen);

        } else {    // Si es un móvil... mostramos imagen en ActivityFotoEspecie
            mostrarActivityFoto(fm);
        }

    }

    private void mostrarActivityFoto(FMarina fm) {
        Intent agrandarImagen = new Intent(this, MainActivity2.class);
        agrandarImagen.putExtra("imagenEspecie",fm.getRef());
        agrandarImagen.putExtra("nombreComunEspecie", fm.getNombre());
        agrandarImagen.putExtra("nombreLatinEspecie", fm.getLatin());

        startActivityForResult(agrandarImagen, IMAGEN_GRANDE);
    }


}
