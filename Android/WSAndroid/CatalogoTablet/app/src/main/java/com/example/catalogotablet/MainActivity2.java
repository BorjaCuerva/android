package com.example.catalogotablet;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity2 extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

// 1. Establecemos el contenido de la actividad a partir del XML.
        setContentView(R.layout.activity_foto);

        // 2. Activar la flechita para volver al activity principal
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 3. Linkamos Fragment:
        FragmentDetalles fragmentDetalles =
                (FragmentDetalles) getSupportFragmentManager()
                        .findFragmentById(R.id.FrgDetalle);

        // 4. Recuperamos los extras que nos llegan
        if (this.getIntent()!=null) {

            int imagenEspecie = this.getIntent().getIntExtra("imagenEspecie", 0);
            String nombreComun = this.getIntent().getStringExtra("nombreComunEspecie");
            String nombreLatin = this.getIntent().getStringExtra("nombreLatinEspecie");

            //5. Cambiamos título del ActionBar al nombre de la especie pulsada en el Home
            getSupportActionBar().setTitle(nombreComun + " (" + nombreLatin + ")");

            fragmentDetalles.mostrarDetalle(imagenEspecie);

    }
}
}
