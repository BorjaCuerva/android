package com.example.borja.catalogo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class AdaptadorFaunaMarina extends ArrayAdapter<Peces> {

    Activity contexto;

    AdaptadorFaunaMarina(Activity contexto, ArrayList<Peces> datos) {
        //Llamamos al constructor de la clase superior
        //se le pasa el xml que genera la fila y el array de objetos
        super(contexto, R.layout.lista, datos);
        this.contexto = contexto;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.lista, null);

        //Mediante getItem cargamos cada uno de los objetos del array
        Peces mielemento = getItem(position);

        ImageView imagen = (ImageView) item.findViewById(R.id.imageView);
        TextView principal = (TextView) item.findViewById(R.id.textPrincipal);
        TextView latin = (TextView) item.findViewById(R.id.textLatinajo);
        TextView tam = (TextView) item.findViewById(R.id.textTam);
        TextView habitat = (TextView) item.findViewById(R.id.textHabitat);

        imagen.setImageResource(mielemento.getReferencia());
        principal.setText(mielemento.getNombre());
        latin.setText(mielemento.getLatin());
        tam.setText(mielemento.getTamanio());
        habitat.setText(mielemento.getHabitat());

        // Devolvemos la Vista (nueva o reutilizada) que dibuja la opcion
        return (item);
    }

}
