package com.example.borja.catalogo;

/**
 * Clase Peces donde tenemos todos los peces con sus caracteristicas
 * @author Borja
 */

public class Peces {

    int referencia;
    String nombre;
    String latin;
    String tamanio;
    String habitat;

    /**
     * Constructor por defecto
     * @param referencia
     * @param nombre
     * @param latin
     * @param tamanio
     * @param habitat
     */
    public Peces(int referencia, String nombre, String latin, String tamanio, String habitat) {
        this.referencia = referencia;
        this.nombre = nombre;
        this.latin = latin;
        this.tamanio = tamanio;
        this.habitat = habitat;
    }

    /**
     * Getters and setters
     */

    public int getReferencia() {
        return referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatin() {
        return latin;
    }

    public void setLatin(String latin) {
        this.latin = latin;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }
}
