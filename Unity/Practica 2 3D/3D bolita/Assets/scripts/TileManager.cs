﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {
	public GameObject[] tilePrefabs;
	public GameObject currentTile;
	private static TileManager instance;
	public static TileManager Instance{
		get{
			if (instance == null) {
				instance = GameObject.FindObjectOfType<TileManager> ();
			}
			return instance;
		}
	}
	// Use this for initialization
	void Start () {

		for (int i = 0; i < 7; i++) {
			CrearBaldosa ();
		}
		

	}


	public void CrearBaldosa ()
	{
		int r=Random.Range (0, 2);

		switch (r) {
		case 0:
			currentTile = (GameObject)Instantiate (tilePrefabs [0], currentTile.transform.GetChild (0).transform.GetChild (0).position, Quaternion.identity);
			break;
		case 1:
			currentTile = (GameObject)Instantiate (tilePrefabs [1], currentTile.transform.GetChild (0).transform.GetChild (1).position, Quaternion.identity);
			break;
		}

		int aparicionObjeto = Random.Range (0, 11);

		if (aparicionObjeto == 0) {
			currentTile.transform.GetChild (1).gameObject.SetActive (true);
		}
	}
		
		
	public void ReiniciarJuego()
	{
		
		Application.LoadLevel(Application.loadedLevel);
	}

	public void exit()
	{
		Application.Quit();
	}
}