﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoPala : MonoBehaviour {

    //Creamos los KeyCode para asignarle las teclas de movimiento
    public KeyCode arriba;
    public KeyCode abajo;

    //Variables para la velocidad del objeto
    private float velocidad;
    private float velocidadFinal;

    private Vector3 posicionInicial;

    // Use this for initialization
    void Start () {

        //Le damos una velocidad inicial
        velocidad = 0.15f;
        posicionInicial = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        // If para el movimiento superior de la pala
        if (Input.GetKey(arriba))
        {
            //Si la pala llega a la posicion 2.12 de y
            if (transform.localPosition.y > 2.50)
            {
                velocidadFinal = 0; //hacemos que su velocidad sea 0 para que se pare
            }
            else
            {
                velocidadFinal = velocidad; //Si no llega a esa posicion, mantenemos la velocidad
            }

            //Le damos a la pala su velocidad final
            transform.Translate(0, velocidadFinal, 0);

        }

        // If para el movimiento inferior de la pala
        if (Input.GetKey(abajo))
        {
            //Si la pala llega a la posicion -2.12 de y
            if (transform.localPosition.y < -0.7500)
            {
                velocidadFinal = 0; //hacemos que su velocidad sea 0 para que se pare
            }
            else
            {
                velocidadFinal = velocidad; //Si no llega a esa posicion, mantenemos la velocidad
            }

            //Le damos a la pala su velocidad final
            transform.Translate(0, -velocidadFinal, 0);

        }



    }

}
