﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bola : MonoBehaviour {

   public Rigidbody rigibola;
    public KeyCode iniciarJuego;
    bool enJuego = true;
    Vector3 impulsoInicial;
    public float velocidadInicial = 15f;

	// Use this for initialization
	void Start () {

        if (Input.GetKey(iniciarJuego))
        {
            enJuego = true;
        }

        if (enJuego == false)
        {
            moverBola();
        }

    }
	
	// Update is called once per frame
	void Update () {

      


    }

    private void moverBola()
    {
        //Le damos la velodidad
        impulsoInicial = new Vector3(-velocidadInicial,velocidadInicial,0);
        rigibola.AddForce(impulsoInicial,ForceMode.Force);
    }
}
