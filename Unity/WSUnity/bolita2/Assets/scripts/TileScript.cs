﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour {

	private float tiempoCaida=1f;


	// Use this for initialization
	void Start () {
		

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			TileManager.Instance.CrearBaldosa ();
			StartCoroutine (caerBaldosa());
		}
	}

	IEnumerator caerBaldosa()
	{
		yield return new WaitForSeconds(tiempoCaida);
		GetComponent<Rigidbody>().isKinematic = false;
		yield return new WaitForSeconds (tiempoCaida);
		Destroy (gameObject);
	}






}
