﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class moverBola : MonoBehaviour {

	public float speed;
	private Vector3 dir;
	public bool direccion;
	private bool isdead;
	public GameObject ps;
	public GameObject menu;
	private int score=0;
	public Text scoreText;
	public Text myScore;
	public Text bestScore;
	public AudioSource audio;
	public AudioSource audio2;


	// Use this for initialization
	void Start () {
		isdead = false;
		direccion = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0) && !isdead) {
			if (direccion) {
				dir = Vector3.forward * speed * Time.deltaTime;
				direccion = false;
			} else {
				dir = Vector3.left * speed * Time.deltaTime;
				direccion = true;
			}
		}
		transform.Translate (dir);
		salidaInicial ();
	}

	void OnTriggerEnter(Collider other){
		score++;
		scoreText.text = score.ToString ();
		if (other.tag == "Objeto") {
			other.gameObject.SetActive (false);
			Instantiate (ps, transform.position, Quaternion.identity);
			score=score +3;
			scoreText.text = score.ToString ();
			audio.Play();
		}
	}

	private void GameOver()
	{
		myScore.text = score.ToString ();

		int bestscore = PlayerPrefs.GetInt ("BestScore", 0);

		if (score > bestscore) {
			PlayerPrefs.SetInt ("BestScore", score);
		}
		bestScore.text = PlayerPrefs.GetInt ("BestScore").ToString();
	}

	void salidaInicial()
	{
		if (transform.position.y < 1f) {
			isdead = true;
			GameOver ();
			menu.SetActive (true);
			if (transform.childCount > 0) {
				transform.GetChild (0).transform.parent = null;
				audio2.Play();
			}
		}
	}

}
