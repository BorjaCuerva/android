﻿
using UnityEngine;

public class camara : MonoBehaviour {
    
    public GameObject player;
    private Vector3 posicionRelativa;
    private Vector3 posicionCaida;

	// Use this for initialization
	void Start () {

        posicionRelativa = transform.position - player.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        //Vamos dando valor a posicion caida = a la posicion actual de la camara
        posicionCaida = transform.position;

        //La camara va siguiendo al jugador
        transform.position = player.transform.position + posicionRelativa;

        //si la posicion del jugador es menor que 0 = cae del escenario
        if (player.transform.position.y < 0)
        {
           //la camara deja de seguir al jugador y se queda en la ultima posicion donde estaba
            transform.position = posicionCaida;
          
        }

	}
}
