﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoBola : MonoBehaviour {

    public float velocidad;
    private Vector3 direccion;
    bool dir;

    // Use this for initialization
    void Start () {


    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            if (dir)
            {
                direccion = Vector3.left;
                dir = false;
            }
            else
            {
                direccion = Vector3.forward;
                dir = true;
            }
        }
        transform.Translate(direccion * Time.deltaTime * velocidad);
        

    }
}
