﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausa : MonoBehaviour
{

    bool active;
    Canvas canvas;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //al pulsar espacio, ponemos el juego en pausa
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            active = !active;
            canvas.enabled = active;
            //condicion ternaria, lo primero despues de la ? es si active es verdadero lo pone a 0
            //Si es falso lo pone a 1
            Time.timeScale = (active) ? 0 : 1f;// 0 tiempo parado, 1 tiempo normal
        }
    }
}
