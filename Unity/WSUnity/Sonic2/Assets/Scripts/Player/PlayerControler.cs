﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour {

    bool canJump;
    public AudioClip sonido = null;
    public Transform posicion = null;
    public float volumen = 1f;


    // Use this for initialization
    void Start () {
        posicion = transform;
    }
	
	// Update is called once per frame
	void Update () {

        /*
         * Si pulsamos el boton izquierda
         */
        if (Input.GetKey("left"))
        {
           
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-700f * Time.deltaTime, 0));
            gameObject.GetComponent<Animator>().SetBool("moving",true);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }

        /*
         * Si pulsamos el boton derecha
         */
        if (Input.GetKey("right"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(700f * Time.deltaTime, 0));
            gameObject.GetComponent<Animator>().SetBool("moving", true);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        /*
         * Si no pulsamos ni izquierda ni derecha, salta la animacion de Sonic parado
         */
        if (!Input.GetKey("left") && !Input.GetKey("right"))
        {
            gameObject.GetComponent<Animator>().SetBool("moving", false);
            
        }
        
        if (!Input.GetKey("up"))
        {
            gameObject.GetComponent<Animator>().SetBool("jump", false);
        }
        
        /*
         * Si pulsamos el boton arriba salta
         */
        if (Input.GetKeyDown("up") && canJump)
        {

             if (sonido)
        {
            AudioSource.PlayClipAtPoint(sonido, posicion.position, volumen);
        }
            canJump = false;
            gameObject.GetComponent<Animator>().SetBool("jump", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,300f));
        }

        if (Input.GetKeyDown("up") && canJump && Input.GetKeyDown("left"))
        {
            canJump = false;
            gameObject.GetComponent<Animator>().SetBool("jump", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300f));
        }

        if (Input.GetKeyDown("up") && canJump && Input.GetKeyDown("right"))
        {
            canJump = false;
            gameObject.GetComponent<Animator>().SetBool("jump", true);
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300f));
        }

    }

        /*
         * Controlamos que solo podamos saltar cuando el player este tocando el suelo
         */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "ground")
        {
            canJump = true;
            
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("MenuInicio");
    }



}
