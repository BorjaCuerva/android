﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour
{
        /*
         * Metodo que destruye el objeto ring cuando el player lo toca
         */

    public AudioClip sonido = null;
    public float volumen = 2f;
    public Transform posicion = null;

    public void Start()
    {
        posicion = transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.name == "suelo")
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.name == "Player")
        {
            VidasManager.vidasManager.RaiseScore(1);
            if (sonido)
            {
                AudioSource.PlayClipAtPoint(sonido, posicion.position, volumen);
            }
            Destroy(this.gameObject);
        }



    }
}
