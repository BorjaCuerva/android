﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeSpawner : MonoBehaviour
{

      float timer; //Tiempo en segundos
        float timerDestroy;
    public GameObject spikePrefab; //Prefab ring


    /*
     * Metodo que hace que los rings spawneen
     */

    void Update()
    {

        timer += Time.deltaTime; //Cada segundo le suma 1

        /*
         * Si el tiempo es = o mayor que 2 segundos reiniciamos el tiempo
         * Generamos un ring aleatorio entre los rangos -5 y 5 de X
         */
        if (timer >= 1f)
        {
            timer = 0; //Reiniciamos el tiempo de respawn
            float x = Random.Range(-5f, 5f); //Posicion aleatoria de respawn
            Vector3 position = new Vector3(x, 4.4f, 0); //Posicion del ring
            Quaternion rotation = new Quaternion(); //Hay que crearlo vacio para que no de error el Instanciate
            Instantiate(spikePrefab, position, rotation); //Instanciamos el ring

        }

    }
}
