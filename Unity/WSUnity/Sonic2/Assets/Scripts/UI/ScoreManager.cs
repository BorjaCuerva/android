﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreManager : MonoBehaviour
{
    public static ScoreManager scoreManager;
    int score = 0;
    public Text scoreText;
    public Text recordText;

    private void Start()
    {
        scoreManager = this;
        recordText.text = "RECORD: " + GetMaxScore().ToString();
    }


    public void RaiseScore(int s)
    {

        score += s;
        scoreText.text = score + "";

        //Si superamos la puntuacion maxima la guardamos
        if (score >= GetMaxScore())
        {
            recordText.text = "RECORD: " + GetMaxScore().ToString();
            SaveScore(score);
        }

    }

    //Ponemos record
    public int GetMaxScore()
    {
        return PlayerPrefs.GetInt("Max Points", 0);
    }

    //Guardamos record
    public void SaveScore(int currentPoints)
    {
        PlayerPrefs.SetInt("Max Points", currentPoints);
    }

}
