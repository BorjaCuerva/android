﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VidasManager : MonoBehaviour
{
    public static VidasManager vidasManager;
    public int vidasResta;
    public Text scoreVidas;

    private void Start()
    {
        vidasManager = this;
        PlayerPrefs.SetInt("vidas",3);
        vidasResta = PlayerPrefs.GetInt("vidas");

    }

    private void Update()
    {
        if (vidasResta == 0)
        {
            RestartGame();
        }
    }

    public void RaiseScore(int s)
    {

        PlayerPrefs.SetInt("vidas",vidasResta-1);
        vidasResta = PlayerPrefs.GetInt("vidas");
        scoreVidas.text = vidasResta + "";

    }

    public void RestartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

}
