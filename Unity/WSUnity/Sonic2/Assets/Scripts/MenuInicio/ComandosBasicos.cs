﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComandosBasicos : MonoBehaviour
{
    
    public void CargarEscena(string nombreDeEscena)
    {
        SceneManager.LoadSceneAsync(nombreDeEscena);
    }

    public void Salir()
    {
        Application.Quit();
        Debug.Log("Has salido");
    }
}
