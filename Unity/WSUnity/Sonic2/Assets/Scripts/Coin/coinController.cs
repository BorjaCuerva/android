﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinController : MonoBehaviour
{
    /*
     * Metodo que destruye el objeto ring cuando el player lo toca
     */

    public AudioClip sonido = null;
    public float volumen = 1f;
    public Transform posicion = null;

    public void Start()
    {
        posicion = transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.name=="suelo")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.name == "Player")
        {
            ScoreManager.scoreManager.RaiseScore(1);
            if (sonido)
            {
                AudioSource.PlayClipAtPoint(sonido, posicion.position, volumen);
            }
            Destroy(this.gameObject);
        }
        
    }
}
